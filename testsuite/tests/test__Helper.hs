-- |
-- Module : Main to test Helper module
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3

module Main where

import Test.HUnit


import Helper



test__s
  = [ "s" ~: "0" ~: ""  ~=? (s 0)
    , "s" ~: "1" ~: ""  ~=? (s 1)
    , "s" ~: "2" ~: "s" ~=? (s 2)
    , "s" ~: "3" ~: "s" ~=? (s 3)
    , "s" ~: "4" ~: "s" ~=? (s 4)
    ]



test__square
  = [ "square" ~: "0"   ~:   0 ~=? (square  0)
    , "square" ~: "1"   ~:   1 ~=? (square  1)
    , "square" ~: "2"   ~:   4 ~=? (square  2)
    , "square" ~: "3"   ~: 256 ~=? (square 16)
    , "square" ~: "-1"  ~:   1 ~=? (square (-1))
    , "square" ~: "-25" ~:  25 ~=? (square (-5))
    ]



test__showMaybe
  = [ "showMaybe" ~: "Nothing" ~: "x" ~=? (showMaybe (Nothing :: Maybe Int))
    , "showMaybe" ~: "(Just 42)" ~: "42" ~=? (showMaybe (Just 42))
    ]



test__stringToInt
  = [ "stringToInt" ~: "0" ~: 0 ~=? (stringToInt "0")
    , "stringToInt" ~: "0.0" ~: -1 ~=? (stringToInt "0.0")
    , "stringToInt" ~: "3.14" ~: -1 ~=? (stringToInt "3.14")

    , "stringToInt" ~: "-666" ~: (-666) ~=? (stringToInt "-666")
    , "stringToInt" ~: "-666.42" ~: (-1) ~=? (stringToInt "-666.42")

    , "stringToInt" ~: "-666x42" ~: (-1) ~=? (stringToInt "-666x42")
    ]



main :: IO Counts

main = do runTestTT $ test test__s
          runTestTT $ test test__square
          runTestTT $ test test__showMaybe
          runTestTT $ test test__stringToInt
