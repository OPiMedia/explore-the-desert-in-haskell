-- |
-- Module : Main to test Position module
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3

module Main where

import Test.HUnit


import qualified Data.Set


import Position



test__aroundPositions
  = [ "aroundPositions" ~: "(42, 666) 0" ~: Data.Set.fromList [] ~=? (aroundPositions (42, 666) 0)
    , "aroundPositions" ~: "(4, 7) 1" ~: Data.Set.fromList [        (4, 6),
                                                                   (3, 7),         (5, 7),
                                                                           (4, 8)
                                                                  ]
      ~=? (aroundPositions (4, 7) 1)
    , "aroundPositions" ~: "(4, 7) 2" ~: Data.Set.fromList [                (4, 5),
                                                                           (3, 6), (4, 6), (5, 6),
                                                                   (2, 7), (3, 7),         (5, 7), (6, 7),
                                                                           (3, 8), (4, 8), (5, 8),
                                                                                   (4, 9)
                                                                  ]
      ~=? (aroundPositions (4, 7) 2)
    ]



test__distance2
  = [ "distance2" ~: "(42, 666) (42, 666)" ~: 0 ~=? (distance2 (42, 666) (42, 666))
    , "distance2" ~: "(42, 666) (42, 667)" ~: 1 ~=? (distance2 (42, 666) (42, 667))
    , "distance2" ~: "(42, 666) (43, 666)" ~: 1 ~=? (distance2 (42, 666) (43, 666))
    , "distance2" ~: "(42, 666) (41, 667)" ~: 2 ~=? (distance2 (42, 666) (41, 667))
    , "distance2" ~: "(42, 666) (32, 669)" ~: 109 ~=? (distance2 (42, 666) (32, 669))
    ]



test__isVisible
  = [ "isVisible" ~: "(42, 666) (42, 666) 3" ~: True ~=? (isVisible (42, 666) (42, 666) 3)
    , "isVisible" ~: "(42, 666) (42, 666) 0" ~: True ~=? (isVisible (42, 666) (42, 666) 0)
    , "isVisible" ~: "(41, 666) (42, 666) 0" ~: False ~=? (isVisible (41, 666) (42, 666) 0)
    , "isVisible" ~: "(41, 666) (42, 666) 1" ~: True ~=? (isVisible (41, 666) (42, 666) 1)
    , "isVisible" ~: "(41, 666) (42, 667) 3" ~: True ~=? (isVisible (41, 666) (42, 667) 3)
    , "isVisible" ~: "(41, 666) (43, 667) 2" ~: False~=? (isVisible (41, 666) (43, 667) 2)
    ]



test__movePosition
  = [ "movePosition" ~: "(0, 0) MoveUp" ~: (0, 0) ~=? (movePosition (0, 0) MoveUp)
    , "movePosition" ~: "(7, 5) MoveUp" ~: (7, 4) ~=? (movePosition (7, 5) MoveUp)

    , "movePosition" ~: "(0, 0) MoveDown" ~: (0, 1) ~=? (movePosition (0, 0) MoveDown)
    , "movePosition" ~: "(7, 5) MoveDown" ~: (7, 6) ~=? (movePosition (7, 5) MoveDown)

    , "movePosition" ~: "(0, 0) MoveLeft" ~: (0, 0) ~=? (movePosition (0, 0) MoveLeft)
    , "movePosition" ~: "(7, 5) MoveLeft" ~: (6, 5) ~=? (movePosition (7, 5) MoveLeft)

    , "movePosition" ~: "(0, 0) MoveRight" ~: (1, 0) ~=? (movePosition (0, 0) MoveRight)
    , "movePosition" ~: "(7, 5) MoveRight" ~: (8, 5) ~=? (movePosition (7, 5) MoveRight)
    ]



test__possibleMovements
  = [ "possibleMovements" ~: "(0, 0)" ~: [MoveDown, MoveRight] ~=? (possibleMovements (0, 0))
    , "possibleMovements" ~: "(7, 0)" ~: [MoveLeft, MoveDown, MoveRight] ~=? (possibleMovements (7, 0))
    , "possibleMovements" ~: "(0, 5)" ~: [MoveUp, MoveDown, MoveRight] ~=? (possibleMovements (0, 5))
    , "possibleMovements" ~: "(7, 5)" ~: [MoveUp, MoveLeft, MoveDown, MoveRight] ~=? (possibleMovements (7, 5))
    ]



test__possiblePositions
  = [ "possiblePositions" ~: "(0, 0)" ~: [(0, 1), (1, 0)] ~=? (possiblePositions (0, 0))
    , "possiblePositions" ~: "(7, 0)" ~: [(6, 0), (7, 1), (8, 0)] ~=? (possiblePositions (7, 0))
    , "possiblePositions" ~: "(0, 5)" ~: [(0, 4), (0, 6), (1, 5)] ~=? (possiblePositions (0, 5))
    , "possiblePositions" ~: "(7, 5)" ~: [(7, 4), (6, 5), (7, 6), (8, 5)] ~=? (possiblePositions (7, 5))
    ]



main :: IO Counts

main = do runTestTT $ test test__aroundPositions
          runTestTT $ test test__distance2
          runTestTT $ test test__isVisible
          runTestTT $ test test__movePosition
          runTestTT $ test test__possibleMovements
          runTestTT $ test test__possiblePositions
