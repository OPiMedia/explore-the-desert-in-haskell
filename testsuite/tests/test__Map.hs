-- |
-- Module : Main to test Map module
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3

module Main where

import Test.HUnit


import Map
import Tile



test__findIndexContains
  = [ "findIndexContains" ~: "[] Water" ~: Nothing ~=? (findIndexContains [] Water)
    , "findIndexContains" ~: "[[Desert]] Water" ~: Nothing ~=? (findIndexContains [[Desert]] Water)

    , "findIndexContains" ~: "[[Water]] Water" ~: Just 0 ~=? (findIndexContains [[Water]] Water)
    , "findIndexContains" ~: "[[Desert], [Water]] Water" ~: Just 1 ~=? (findIndexContains [[Desert], [Water]] Water)
    , "findIndexContains" ~: "[[Desert], [Water, Desert, Lava]] Water" ~: Just 1 ~=? (findIndexContains [[Desert], [Water, Desert, Lava]] Lava)
    ]



main :: IO Counts

main = runTestTT $ test test__findIndexContains
