-- |
-- Module : Main to test Player module
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3

module Main where

import Test.HUnit


import Player
import Position



test__move
  = [ "move" ~: "((0, 0), 0, 0, 10, 3) MoveUp" ~: ((0, 0), 0, 0, 10, 3) ~=? (move ((0, 0), 0, 0, 10, 3) MoveUp)
    , "move" ~: "((7, 5), 0, 3, 10, 3) MoveUp" ~: ((7, 4), 0, 2, 10, 3) ~=? (move ((7, 5), 0, 3, 10, 3) MoveUp)

    , "move" ~: "((7, 5), 0, 3, 10, 3) MoveRight" ~: ((8, 5), 0, 2, 10, 3) ~=? (move ((7, 5), 0, 3, 10, 3) MoveRight)
    ]



main :: IO Counts

main = runTestTT $ test test__move
