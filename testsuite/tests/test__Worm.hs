-- |
-- Module : Main to test Worm module
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3

module Main where

import Test.HUnit

import Data.Set


import Position
import Worm



freePositions = Data.Set.fromList [(x, y) | x <- [0..100], y <- [0..100]]


w1 = createWorm (666, 42) MoveLeft 1
w2 = createWorm (100, 15) MoveDown 2
w3 = createWorm (10, 5) MoveRight 3
w4 = createWorm (5, 36) MoveUp 4
w5 = createWorm (2, 10) MoveLeft 5


iw1 = iterate (updateWorm freePositions) w1
iw2 = iterate (updateWorm freePositions) w2
iw3 = iterate (updateWorm freePositions) w3
iw4 = iterate (updateWorm freePositions) w4
iw5 = iterate (updateWorm freePositions) w5



test__createWorm
  = [ "createWorm" ~: "w1" ~: ((666, 42), (666, 42), (666, 42), (666, 42),
                               MoveLeft, 1, False) ~=? w1
    , "createWorm" ~: "w2" ~: ((100, 15), (100, 15), (100, 15), (100, 14),
                               MoveDown, 1, True) ~=? w2
    , "createWorm" ~: "w3" ~: ((10, 5), (10, 5), (10, 5), (8, 5),
                               MoveRight, 2, True) ~=? w3
    , "createWorm" ~: "w4" ~: ((5, 36), (5, 36), (5, 36), (5, 39),
                               MoveUp, 3, True) ~=? w4
    , "createWorm" ~: "w5" ~: ((2, 10), (2, 10), (2, 10), (6, 10),
                               MoveLeft, 4, True) ~=? w5
    ]



test__updateWorm
  = [ "updateWorm" ~: "w1" ~: ((665, 42), (666, 42), (665, 42), (665, 42),
                               MoveLeft, 0, False) ~=? (updateWorm freePositions w1)

    , "updateWorm"   ~: "w2" ~: ((100, 16), (100, 16), (100, 15), (100, 15),
                                 MoveDown, 2, False) ~=? (updateWorm freePositions w2)
    , "updateWorm^2" ~: "w2" ~: ((100, 17), (100, 16), (100, 16), (100, 16),
                                 MoveDown, 1, False) ~=? (iw2 !! 2)
    , "updateWorm^3" ~: "w2" ~: ((100, 18), (100, 16), (100, 17), (100, 17),
                                 MoveDown, 0, False) ~=? (iw2 !! 3)

    , "updateWorm"   ~: "w3" ~: ((11, 5), (11, 5), (10, 5), (9, 5),
                                 MoveRight, 1, True) ~=? (updateWorm freePositions w3)
    , "updateWorm^2" ~: "w3" ~: ((12, 5), (12, 5), (10, 5), (10, 5),
                                 MoveRight, 3, False) ~=? (iw3 !! 2)
    , "updateWorm^3" ~: "w3" ~: ((13, 5), (12, 5), (11, 5), (11, 5),
                                 MoveRight, 2, False) ~=? (iw3 !! 3)
    , "updateWorm^4" ~: "w3" ~: ((14, 5), (12, 5), (12, 5), (12, 5),
                                 MoveRight, 1, False) ~=? (iw3 !! 4)
    , "updateWorm^5" ~: "w3" ~: ((15, 5), (12, 5), (13, 5), (13, 5),
                                 MoveRight, 0, False) ~=? (iw3 !! 5)

    , "updateWorm"   ~: "w4" ~: ((5, 35), (5, 35), (5, 36), (5, 38),
                                 MoveUp, 2, True) ~=? (updateWorm freePositions w4)
    , "updateWorm^2" ~: "w4" ~: ((5, 34), (5, 34), (5, 36), (5, 37),
                                 MoveUp, 1, True) ~=? (iw4 !! 2)
    , "updateWorm^3" ~: "w4" ~: ((5, 33), (5, 33), (5, 36), (5, 36),
                                 MoveUp, 4, False) ~=? (iw4 !! 3)
    , "updateWorm^4" ~: "w4" ~: ((5, 32), (5, 33), (5, 35), (5, 35),
                                 MoveUp, 3, False) ~=? (iw4 !! 4)
    , "updateWorm^5" ~: "w4" ~: ((5, 31), (5, 33), (5, 34), (5, 34),
                                 MoveUp, 2, False) ~=? (iw4 !! 5)
    , "updateWorm^6" ~: "w4" ~: ((5, 30), (5, 33), (5, 33), (5, 33),
                                 MoveUp, 1, False) ~=? (iw4 !! 6)
    , "updateWorm^7" ~: "w4" ~: ((5, 29), (5, 33), (5, 32), (5, 32),
                                 MoveUp, 0, False) ~=? (iw4 !! 7)

    , "updateWorm"   ~: "w5" ~: ((1, 10), (1, 10), (2, 10), (5, 10),
                                 MoveLeft, 3, True) ~=? (updateWorm freePositions w5)
    , "updateWorm^2" ~: "w5" ~: ((0, 10), (0, 10), (2, 10), (4, 10),
                                 MoveLeft, 2, True) ~=? (iw5 !! 2)
    , "updateWorm^3" ~: "w5" ~: ((-1, 10), (0, 10), (2, 10), (3, 10),
                                 MoveLeft, 4, False) ~=? (iw5 !! 3)
    , "updateWorm^4" ~: "w5" ~: ((-2, 10), (0, 10), (2, 10), (2, 10),
                                 MoveLeft, 3, False) ~=? (iw5 !! 4)
    , "updateWorm^5" ~: "w5" ~: ((-3, 10), (0, 10), (1, 10), (1, 10),
                                 MoveLeft, 2, False) ~=? (iw5 !! 5)
    , "updateWorm^6" ~: "w5" ~: ((-4, 10), (0, 10), (0, 10), (0, 10),
                                 MoveLeft, 1, False) ~=? (iw5 !! 6)
    , "updateWorm^7" ~: "w5" ~: ((-5, 10), (0, 10), (0, 10), (-1, 10),
                                 MoveLeft, 0, False) ~=? (iw5 !! 7)
    ]



main :: IO Counts

main = do runTestTT $ test test__createWorm
          runTestTT $ test test__updateWorm
