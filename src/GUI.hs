-- |
-- Module : GUI
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- GUI functions.

module GUI
  ( drawAll
  , loadImages
  , windowHeight
  , windowWidth
  ) where

import qualified Data.Set


import qualified Graphics.Gloss.Data.Bitmap as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss

import Game
import Helper
import Map
import Player
import Position
import Tile
import Worm



type Images = (TileImages, PlayerImages, WormImages)

type PlayerImages = (Gloss.Picture, Gloss.Picture, Gloss.Picture, Gloss.Picture, Gloss.Picture)
type TileImages = (Gloss.Picture, Gloss.Picture, Gloss.Picture, Gloss.Picture, Gloss.Picture)
type WormImages = (Gloss.Picture, Gloss.Picture, Gloss.Picture)



mapHeight :: Int
mapHeight = 800

mapWidth :: Int
mapWidth = 800


windowHeight :: Int
windowHeight = 900

windowWidth :: Int
windowWidth = 800


tileSize :: Float
tileSize = 20



-- | Draw all the game.
drawAll :: Game -> Images -> IO Gloss.Picture

drawAll game@( _, desertMap, player
             , _, viewedPositions, _
             , _, _, worms)
        images
  = return (Gloss.pictures [ Gloss.translate ((tileSize - fromIntegral mapWidth) / 2)
                             ((fromIntegral windowHeight- tileSize) / 2)
                             $ drawMapAndContent desertMap viewedPositions player worms images
                           , Gloss.translate 0 (-fromIntegral mapHeight / 2) $ drawInfos game])



-- | Draw the panel with information.
drawInfos :: Game -> Gloss.Picture
drawInfos ( state, desertMap, (pos, treasure, water, maxWater, _)
          , _, _, _
          , _, _, worms)
  = let reachableTiles = bfsReachableTiles desertMap pos
    in Gloss.pictures
       [Gloss.color Gloss.white
         $ Gloss.rectangleSolid (fromIntegral windowWidth) (fromIntegral (windowHeight - mapHeight))
       , Gloss.translate (10 -fromIntegral windowWidth / 2) 25
         $ Gloss.Scale 0.15 0.15
         $ Gloss.pictures [ Gloss.Text $ show pos
                            ++ "   Water: " ++ show water ++ " / " ++ show maxWater
                            ++ "   Treasure: " ++ show treasure
                            ++ "   \"Accessible\" worms: " ++ show (length worms)
                          , Gloss.translate 0 (-200)
                            (Gloss.Text $ "Shortest path to Lava: "
                              ++ showMaybe (findIndexContains reachableTiles Lava)
                              ++ "   to Water: "
                              ++ showMaybe (findIndexContains reachableTiles Water)
                              ++ "   to Treasure: "
                              ++ showMaybe (findIndexContains reachableTiles Treasure)
                              ++ "   to Portal: "
                              ++ showMaybe (findIndexContains reachableTiles Portal))
                          , Gloss.translate 0 (-400)
                            (Gloss.Text $ "State: " ++ show state)]]



-- | Draw all the map (only tiles).
drawMap :: Map -> ViewedPositions -> TileImages -> Gloss.Picture
drawMap desertMap viewedPositions images
  = Gloss.pictures $ map (\ pos -> drawTilePos (positionToTile desertMap pos) images pos)
                         (Data.Set.toList viewedPositions)



-- | Draw all the map with player and worms.
drawMapAndContent :: Map -> ViewedPositions -> Player -> Worms -> Images -> Gloss.Picture
drawMapAndContent desertMap viewedPositions player@(playerPosition, _, _, _, _) worms
                  (tileImages, playerImages, wormImages)
  = let (x, y) = positionToGlossXY playerPosition
    in Gloss.translate ((fromIntegral mapWidth - tileSize)/2 - x)
                       (-(fromIntegral mapHeight - tileSize)/2 - y)
       $ Gloss.pictures [drawMap desertMap viewedPositions tileImages
                        , drawPlayer desertMap player playerImages
                        , drawWorms worms wormImages viewedPositions]



-- | Draw the player.
drawPlayer :: Map -> Player -> PlayerImages -> Gloss.Picture

drawPlayer desertMap (position, _, water, _, _)
           (playerDesert, playerWater, playerWinPortal , playerDeadDesert, playerDeadLava)
  = let (gX, gY) = positionToGlossXY position
        tile = positionToTile desertMap position
    in Gloss.translate gX gY $ if tile == Lava
                               then playerDeadLava
                               else if tile == Water
                                    then playerWater
                                    else if tile == Portal
                                         then playerWinPortal
                                         else if water == 0
                                              then playerDeadDesert
                                              else playerDesert



-- | Draw one tile.
drawTile :: Tile -> TileImages -> Gloss.Picture

drawTile Desert   (desert, _, _, _, _)   = desert
drawTile Lava     (_, lava, _, _, _)     = lava
drawTile Portal   (_, _, portal, _, _)   = portal
drawTile Water    (_, _, _, water, _)    = water
drawTile Treasure (_, _, _, _, treasure) = treasure



-- | Draw one tile to the given position.
drawTilePos :: Tile -> TileImages -> Position -> Gloss.Picture

drawTilePos tile images pos = let (gX, gY) = positionToGlossXY pos
                              in Gloss.translate gX gY $ drawTile tile images



-- | Draw all worms.
drawWorms :: Worms -> WormImages -> ViewedPositions -> Gloss.Picture
drawWorms worms (headImage, bodyImage, endImage) viewedPositions
  = Gloss.pictures
    [Gloss.pictures
      $ map (\ worm -> drawWormHeadIf worm headImage viewedPositions) worms
    , drawWormsBodySegments worms bodyImage viewedPositions
    , Gloss.pictures
      $ map (\ worm -> drawWormEndIf worm endImage viewedPositions) worms]



-- | Draw end head worm.
drawWormEndIf :: Worm -> Gloss.Picture -> ViewedPositions -> Gloss.Picture
drawWormEndIf (_, _, lastVisiblePos, endPos, movement, _, _) image viewedPositions
  = if endPos == lastVisiblePos && endPos `elem` viewedPositions
    then let (gX, gY) = positionToGlossXY endPos
         in Gloss.translate gX gY $ rotateWormHeadOrEnd movement image
    else Gloss.blank



-- | Draw one head worm.
drawWormHeadIf :: Worm -> Gloss.Picture -> ViewedPositions -> Gloss.Picture
drawWormHeadIf (headPos, firstVisiblePos, _, _, movement, _, _) image viewedPositions
  = if headPos == firstVisiblePos && headPos `elem` viewedPositions
    then let (gX, gY) = positionToGlossXY headPos
         in Gloss.translate gX gY $ rotateWormHeadOrEnd movement image
    else Gloss.blank



-- | Draw one worm body segment.
drawWormBodySegment :: Position -> Gloss.Picture -> Gloss.Picture
drawWormBodySegment pos image
  = let (gX, gY) = positionToGlossXY pos
    in Gloss.translate gX gY image



-- | Draw all body segments of all worms.
drawWormsBodySegments :: Worms -> Gloss.Picture -> ViewedPositions -> Gloss.Picture
drawWormsBodySegments worms image viewedPositions
  = Gloss.pictures
    (map (`drawWormBodySegment` image)
      (Data.Set.toList
        $ Data.Set.intersection (Data.Set.fromList $ concatMap wormVisibleBodyPositions worms)
                                viewedPositions))



-- | Load and return images.
loadImages :: IO Images

loadImages = do putStrLn "Loading images..."
                desert   <- Gloss.loadBMP "img/desert.bmp"
                lava     <- Gloss.loadBMP "img/lava.bmp"
                portal   <- Gloss.loadBMP "img/portal.bmp"
                water    <- Gloss.loadBMP "img/water.bmp"
                treasure <- Gloss.loadBMP "img/treasure.bmp"

                playerDesert <- Gloss.loadBMP "img/player_desert.bmp"
                playerWater  <- Gloss.loadBMP "img/player_water.bmp"
                playerWinPortal <- Gloss.loadBMP "img/player_win_portal.bmp"
                playerDeadDesert <- Gloss.loadBMP "img/player_dead_desert.bmp"
                playerDeadLava   <- Gloss.loadBMP "img/player_dead_lava.bmp"

                wormHead <- Gloss.loadBMP "img/worm_head.bmp"
                wormBody <- Gloss.loadBMP "img/worm_body.bmp"
                wormEnd  <- Gloss.loadBMP "img/worm_end.bmp"

                putStrLn "Images loaded"
                return ((desert, lava, portal, water, treasure)
                       , ( playerDesert, playerWater, playerWinPortal
                          , playerDeadDesert, playerDeadLava)
                       , (wormHead, wormBody, wormEnd))



-- | Convert a position on the map to a position for Gloss.
positionToGlossXY :: Position -> (Float, Float)

positionToGlossXY (x, y) = (fromIntegral x * tileSize,
                            - fromIntegral y * tileSize)



-- | Rotate the head or end image corresponding to the movement of the worm.
rotateWormHeadOrEnd :: Movement -> Gloss.Picture -> Gloss.Picture

rotateWormHeadOrEnd MoveDown  image = Gloss.rotate 180 image
rotateWormHeadOrEnd MoveRight image = Gloss.rotate  90 image
rotateWormHeadOrEnd MoveLeft  image = Gloss.rotate 270 image
rotateWormHeadOrEnd _ image = image
