-- |
-- Module : Position
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Basic type of 2D integer positions, with functions to move.

module Position
  ( Movement(MoveDown,MoveLeft,MoveRight,MoveUp)
  , Position
  , ViewedPositions
  , aroundPositions
  , distance2
  , distanceLine
  , extendAllPossibleAroundPositions
  , isVisible
  , movePosition
  , movePositionAlways
  , possibleMovements
  , possiblePositions
  , reverseMovement
  ) where

import qualified Control.Exception.Base
import qualified Data.Set



import Helper



-- | Integer position (x, y).
type Position = (Int, Int)


-- | 4 possible movements.
data Movement = MoveDown | MoveLeft | MoveRight | MoveUp deriving (Eq, Ord, Show)


-- | Set of positions already viewed.
type ViewedPositions = Data.Set.Set Position



-- | Set of positions at the given distance from the given position (without this position itself).
aroundPositions :: Position -> Int -> ViewedPositions

aroundPositions pos@(x, y) len
  = Data.Set.fromList [(x + dx, y + dy)
                      | dx <- [-len..len], dy <- [-len..len],
                        x + dx >= 0, y + dy >= 0,
                        (dx /= 0) || (dy /= 0),
                        distance2 pos (x + dx, y + dy) <= square len]



-- | The square of the distance between the two positions.
distance2 :: Position -> Position -> Int

distance2 (x0, y0) (x1, y1) = square (x0 - x1) + square (y0 - y1)



-- | The distance between the two positions.
-- The two positions must be in the same line or in the same column.
distanceLine :: Position -> Position -> Int

distanceLine (x0, y0) (x1, y1) = abs (x0 - x1) + abs (y0 - y1)



-- | For each position add all positions at the given distance from this position.
extendAllPossibleAroundPositions :: ViewedPositions -> Int -> ViewedPositions

extendAllPossibleAroundPositions viewPositions len
  = Data.Set.foldr (\ position acc -> Data.Set.union acc $ aroundPositions position len)
                   viewPositions
                   viewPositions



-- | True iff pos is in the line of sight of currentPos.
isVisible :: Position -> Position -> Int -> Bool

isVisible currentPos pos radius = distance2 currentPos pos <= radius*radius



-- | Apply a Movement on a Position
-- and return a valid Position.
movePosition :: Position -> Movement -> Position

movePosition (x, y) MoveUp    = Control.Exception.Base.assert (x >= 0)
                                Control.Exception.Base.assert (y >= 0)
                                (x, max 0 (y - 1))
movePosition (x, y) MoveDown  = Control.Exception.Base.assert (x >= 0)
                                Control.Exception.Base.assert (y >= 0)
                                (x, y + 1)

movePosition (x, y) MoveLeft  = Control.Exception.Base.assert (x >= 0)
                                Control.Exception.Base.assert (y >= 0)
                                (max 0 (x - 1), y)
movePosition (x, y) MoveRight = Control.Exception.Base.assert (x >= 0)
                                Control.Exception.Base.assert (y >= 0)
                                (x + 1,         y)



-- | Apply a Movement on a Position
-- even if out of the map.
movePositionAlways :: Position -> Movement -> Position

movePositionAlways (x, y) MoveUp    = (x,     y - 1)
movePositionAlways (x, y) MoveDown  = (x,     y + 1)
movePositionAlways (x, y) MoveLeft  = (x - 1, y)
movePositionAlways (x, y) MoveRight = (x + 1, y)



-- | List of possible Movement for this Position, i.e. Movement that do not go out of the map.
possibleMovements :: Position -> [Movement]

possibleMovements (x, y)
  = [MoveUp | y > 0] ++ [MoveLeft | x > 0] ++ [MoveDown, MoveRight]



-- | List of possible Position reachable from the given Position.
possiblePositions :: Position -> [Position]

possiblePositions pos = [movePosition pos move | move <- possibleMovements pos]



-- | Reverse the movement
reverseMovement :: Movement -> Movement

reverseMovement MoveUp    = MoveDown
reverseMovement MoveDown  = MoveUp
reverseMovement MoveLeft  = MoveRight
reverseMovement MoveRight = MoveLeft
