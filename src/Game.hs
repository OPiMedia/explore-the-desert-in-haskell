-- |
-- Module : Game
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Type and update function for the complete combination of all elements of the game.

module Game
  ( Game
  , Params
  , State(Living, DeadLava, DeadThirst, DeadWorm, Win, Exit)
  , changeState
  , concurrentUpdateWorm
  , createNewGame
  , getMap
  , getPlayer
  , getViewedPositions
  , getWorms
  , isLiving
  , moveUpdate
  ) where

import qualified Control.Concurrent
import qualified Control.Concurrent.STM
import qualified Control.Monad
import qualified Data.List
import qualified Data.Set
import qualified System.Random


import Map
import Player
import Position
import Tile
import Worm



-- | All data for the game:
-- (_, _, _, maximum visible position, collected treasure positions, worm size, worm likelihood, _)
type Game = (State, Map, Player, Position, ViewedPositions, [Position], Int, [Bool], Worms)


-- | All initial parameters:
-- (s, m, g, t, w, p, l, ll, x, y)
type Params = (Int, Int, Int, Int, Int, Int, Int, Int, Int, Int)



-- | Type for the state of the player.
data State = Living | DeadLava | DeadThirst | DeadWorm | Win | Exit deriving (Eq, Ord)


instance Show State where
  show Living     = "You are still alive."
  show DeadLava   = "You DIED burned by the lava!"
  show DeadThirst = "You are DEAD of thirst!"
  show DeadWorm   = "You were KILLED by a worm"
  show Win        = "You WIN."
  show _ = "Bye!"



-- | Change the state of the game.
changeState :: Game -> State -> Game

changeState (_, desertMap, player
            , (maxVisibleX, maxVisibleY), viewedPositions, collectedTreasures
            , wormSize, createWormBools, worms)
            newState
  = (newState, desertMap, player
    , (maxVisibleX, maxVisibleY), viewedPositions, collectedTreasures
    , wormSize, createWormBools, worms)



-- | Take freePositionsForWormBox and move the worm corresponding to these free positions.
-- Loop until completely disappeared.
concurrentUpdateWorm :: Worm
                     -> Control.Concurrent.STM.TMVar (Int, Data.Set.Set Position)
                     -> Control.Concurrent.STM.TMVar ()
                     -> Control.Concurrent.STM.TMVar Int
                     -> Control.Concurrent.STM.TMVar Worms
                     -> IO ()

concurrentUpdateWorm startedWorm
                     freePositionsForWormBox
                     isFinishedWormsPhaseBox
                     releaseThreadsBox
                     updatedWormsBox
  = loop startedWorm
  where loop :: Worm -> IO ()

        loop worm
          = do let wormVisiblePoss = Data.Set.fromList $ wormVisiblePositions worm

               -- Barrier, wait required information
               (nbWormToUpdate, freePoss)
                 <- Control.Concurrent.STM.atomically
                    $ Control.Concurrent.STM.takeTMVar freePositionsForWormBox

               -- Update worm
               let updatedWorm@(_, _, _, _, _, updatedLifespan, _) = updateWorm freePoss worm
               let updatedFreePoss
                     = Data.Set.union (Data.Set.difference freePoss wormVisiblePoss)
                                      (Data.Set.fromList $ wormVisiblePositions updatedWorm)

               Control.Monad.when (updatedLifespan > 0) $  -- still visible, add to list
                 do updatedWorms <- Control.Concurrent.STM.atomically
                                      $ Control.Concurrent.STM.takeTMVar updatedWormsBox
                    Control.Concurrent.STM.atomically
                      $ Control.Concurrent.STM.putTMVar updatedWormsBox (updatedWorm : updatedWorms)

               if nbWormToUpdate > 1                      -- if other worm must be updated
               then do Control.Concurrent.STM.atomically
                         $ Control.Concurrent.STM.putTMVar freePositionsForWormBox
                                                           (nbWormToUpdate - 1, updatedFreePoss)
                       -- Wait all other worms are finished
                       nbWormToUpdateWaiting <- Control.Concurrent.STM.atomically
                                          $ Control.Concurrent.STM.takeTMVar releaseThreadsBox
                       if nbWormToUpdateWaiting > 1            -- if other worm wait
                       then Control.Concurrent.STM.atomically
                              $ Control.Concurrent.STM.putTMVar releaseThreadsBox
                                                                (nbWormToUpdateWaiting - 1)
                       else Control.Concurrent.STM.atomically  -- or is it was the last waiting
                              $ Control.Concurrent.STM.putTMVar isFinishedWormsPhaseBox ()
               else Control.Concurrent.STM.atomically  -- or is it was the last to be updated
                      $ Control.Concurrent.STM.putTMVar isFinishedWormsPhaseBox ()
               -- Finished critic update part

               Control.Monad.when (updatedLifespan > 0) $ loop updatedWorm  -- loop
               -- Or finish this thread



-- | Return a new game with given parameters.
createNewGame :: Params -> Bool -> Game

createNewGame (s', m, g, t, w, p, l, ll, wormX, wormY) visible
  = let (seed0:seed1:_) = System.Random.randoms $ System.Random.mkStdGen g :: [Int]
        randomBools = map (< wormY)
                      (System.Random.randomRs (0, 100) $ System.Random.mkStdGen seed0)
        initialMaxVisible = if visible
                            then (40 - 1 - s', 40 - 1 - s')
                            else (0, 0)
        initialViewedPositions = Data.Set.union (Data.Set.singleton (0,0))
                                                (Data.Set.filter (\ (x, y) -> not visible
                                                                   || (x < 40) && (y < 40))
                                                 (aroundPositions (0, 0)
                                                  (if visible
                                                   then 56
                                                   else s')))
    in (Living, randomMap seed1 (t, w, p, l, ll),
         ((0, 0), 0, m, m, s'),
         initialMaxVisible, initialViewedPositions, [],
         wormX, randomBools, [])



-- | Return the map.
getMap :: Game -> Map

getMap (_, desertMap, _, _, _, _ , _, _, _) = desertMap



-- | Return the player.
getPlayer :: Game -> Player

getPlayer (_, _, player, _, _, _ , _, _, _) = player



-- | Return the worms.
getWorms :: Game -> Worms

getWorms (_, _, _, _, _, _ , _, _, worms) = worms



-- | Return the viewed positions.
getViewedPositions :: Game -> ViewedPositions

getViewedPositions (_, _, _, _, viewedPositions, _ , _, _, _) = viewedPositions



-- | True iff the state is Living.
isLiving :: Game -> Bool

isLiving (state, _, _, _, _, _ , _, _, _) = state == Living



-- | Get the current game,
-- apply the movement to the player
-- and return the updated state of the game and True iff the state was changed.
movePlayerUpdate :: Game -> Movement -> (Game, Bool)

movePlayerUpdate game@(state, desertMap, player@(pos, _, _, _, _)
                      , (maxVisibleX, maxVisibleY), viewedPositions, collectedTreasures
                      , wormSize, createWormBools, worms)
                 movement
  = let (updatedPos@(updatedX, updatedY), treasure, water, maxWater, radius) = move player movement
    in if updatedPos == pos  -- if impossible movement
       then (game, False)
       else let tile = positionToTile desertMap updatedPos
                isTreasure = tile == Treasure
                updatedDesertMap = if isTreasure  -- remove treasure from the map
                                   then changeTile desertMap updatedPos Desert
                                   else desertMap
                updatedCollectedTreasures = if isTreasure  -- add position to collected treasures
                                            then updatedPos:collectedTreasures
                                            else collectedTreasures
                updatedTreasure = if isTreasure  -- increment number of collected treasures
                                  then treasure + 1
                                  else treasure
                updatedWater = if tile == Water  -- fill stock of water
                               then maxWater
                               else water
                updatedViewedPositions  -- add updated viewed positions from the current updatedPos
                  = Data.Set.union viewedPositions (aroundPositions updatedPos radius)
            in ((state, updatedDesertMap,
                 (updatedPos, updatedTreasure, updatedWater, maxWater, radius),
                 (maximum [maxVisibleX, updatedX],  -- adapt visible zone
                  maximum [maxVisibleY, updatedY]),
                 updatedViewedPositions, updatedCollectedTreasures,
                 wormSize, createWormBools, worms),
                True)



-- | Get the current game,
-- apply the movement the player,
-- update worms, create new worms,
-- and return the updated state of the game.
moveUpdate :: Game -> Movement
           -> Control.Concurrent.STM.TMVar (Int, Data.Set.Set Position)
           -> Control.Concurrent.STM.TMVar ()
           -> Control.Concurrent.STM.TMVar Int
           -> Control.Concurrent.STM.TMVar Worms
           -> IO Game

moveUpdate game movement
           freePositionsForWormBox
           isFinishedWormsPhaseBox
           releaseThreadsBox
           updatedWormsBox
  = let ( ( state, updatedDesertMap, updatedPlayer@(updatedPlayerPos, _, _, _, _)
          , updatedMaxVisible, updatedViewedPositions, updatedCollectedTreasures
          , wormSize, createWormBools, worms
          )
          , changed) = movePlayerUpdate game movement
    in if changed
       then let freeDesertPoss = freeDesertVisiblePositions updatedDesertMap updatedViewedPositions
                                                            worms wormSize
                nbWorm = length worms
            in do -- Updated existing worms
                  Control.Concurrent.STM.atomically
                    $ Control.Concurrent.STM.putTMVar updatedWormsBox []

                  Control.Monad.unless (null worms) $
                    do -- Start concurrent worms update phase
                       Control.Concurrent.STM.atomically
                         $ Control.Concurrent.STM.putTMVar freePositionsForWormBox
                                                           (nbWorm, freeDesertPoss)
                       -- Wait all worms finished updated
                       Control.Concurrent.STM.atomically
                         $ Control.Concurrent.STM.takeTMVar isFinishedWormsPhaseBox

                       Control.Monad.when (nbWorm > 1) $
                         do -- Tell all worms (without the last) to continue
                            Control.Concurrent.STM.atomically
                              $ Control.Concurrent.STM.putTMVar releaseThreadsBox (nbWorm - 1)

                            -- Wait all worms finished waiting
                            Control.Concurrent.STM.atomically
                              $ Control.Concurrent.STM.takeTMVar isFinishedWormsPhaseBox

                  updatedWorms <- Control.Concurrent.STM.atomically
                                    $ Control.Concurrent.STM.takeTMVar updatedWormsBox

                  -- Create new worms
                  let possiblePoss
                        = Data.Set.toList
                          $ Data.Set.difference
                            (freeDesertVisiblePositions updatedDesertMap updatedViewedPositions
                                                        updatedWorms wormSize)
                            (Data.Set.singleton updatedPlayerPos)
                  -- Positions where create new worms
                  let (newWormPositions, _) = unzip $ filter snd $ zip possiblePoss createWormBools
                  let updatedCreateWormBools = drop (Data.List.length possiblePoss) createWormBools

                  -- Create new worms and associate threads
                  let newWorms = map (\ p -> createWorm p (betterMove_ p updatedPlayerPos) wormSize)
                                     newWormPositions
                  mapM_ (\ worm -> Control.Concurrent.forkIO
                                   $ concurrentUpdateWorm worm
                                                          freePositionsForWormBox
                                                          isFinishedWormsPhaseBox
                                                          releaseThreadsBox
                                                          updatedWormsBox)
                        newWorms

                  let allUpdatedWorms = updatedWorms ++ newWorms
                  return $ allUpdatedWorms `seq`
                            stateUpdate_ ( state, updatedDesertMap, updatedPlayer
                                         , updatedMaxVisible
                                         , updatedViewedPositions, updatedCollectedTreasures
                                         , wormSize, updatedCreateWormBools, allUpdatedWorms)
       else return game



--
-- Private
---

-- | The "better" movement for a worm to go from (x0, y0) to (x1, y1).
betterMove_ :: Position -> Position -> Movement

betterMove_ (x0, y0) (x1, y1)
  | abs (x0 - x1) <= abs (y0 - y1) = if y0 > y1
                                     then MoveUp
                                     else MoveDown
  | x0 > x1 = MoveLeft
  | otherwise = MoveRight



stateUpdate_ :: Game -> Game

stateUpdate_ game@( _, desertMap, (pos, _, water, _, _)
                  , _, _, _
                  , _, _, worms)
  = let tile = positionToTile desertMap pos
    in if water == 0
       then changeState game DeadThirst
       else if tile == Lava
       then changeState game DeadLava
       else if tile == Portal
       then changeState game Win
       else if isInWorms pos worms
       then changeState game DeadWorm
       else game
