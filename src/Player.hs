-- |
-- Module : Player
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Type of a player, with its intrinsic information.

module Player
  ( Player
  , playerGetPosition
  , move
  ) where

import qualified Control.Exception.Base


import Position



-- | Type for stock of water.
type WaterCount = Int


-- | Type for number of collected treasure.
type TreasureCount = Int


-- | Type for player: (_, _, Stock of water, Maximum capacity for stock of water, line of sight)
type Player = (Position, TreasureCount, WaterCount, WaterCount, Int)



-- | Apply a Movement on a Player and decrease the stock of water.
-- The stock of water must be NOT empty.
move :: Player -> Movement -> Player

move (pos, treasure, water, maxWater, radius) movement
  = let newPos = movePosition pos movement
    in Control.Exception.Base.assert (fst newPos >= 0)
       Control.Exception.Base.assert (snd newPos >= 0)
       Control.Exception.Base.assert (treasure >= 0)
       Control.Exception.Base.assert (water >= 0)
       Control.Exception.Base.assert (maxWater >= water)
       Control.Exception.Base.assert (radius > 0)
       (newPos, treasure, if newPos == pos then water else water - 1, maxWater, radius)



-- | Return the position.
playerGetPosition :: Player -> Position

playerGetPosition (pos, _, _, _, _) = pos
