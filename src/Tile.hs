-- |
-- Module : Tile
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Type of tile.

module Tile
  ( Tile(Desert, Lava, Portal, Treasure, Water)
  , nbTypeTile
  ) where



-- | Type of tile.
data Tile = Desert | Lava | Portal | Treasure | Water deriving (Eq, Ord)



-- | Number of different types of tile.
nbTypeTile :: Int

nbTypeTile = 5
