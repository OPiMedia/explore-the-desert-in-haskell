{-# LANGUAGE CPP #-}
-- |
-- Module : Main
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Explore the Desert (Part 2): Functional Programming project.

module Main where

import qualified Control.Concurrent
import qualified Control.Concurrent.STM
import qualified Data.Maybe
import qualified System.Environment
import qualified System.Exit
import qualified System.IO


import qualified Graphics.Gloss.Interface.IO.Game as Gloss


import Game
import GameParser
import GUI
import Load
import Position
import Save



-- | Print help message and exit.
helpUsage :: IO ()

helpUsage
  = do putStrLn "Usage: desert s m g t w p l ll [--visible]"
       putStrLn ""
       putStrLn "  s: \"radius\", length of explorer sight (integer >= 0)"
       putStrLn "  m: maximum capacity for stock of water (integer > 0)"
       putStrLn "  g: initial random seed (integer)"
       putStrLn "  t: % likelihood of Treasure *in Desert* (0 < integer <= 100)"
       putStrLn "  w: % likelihood of Water (integer > 0)"
       putStrLn "  p: % likelihood of Portal (integer > 0)"
       putStrLn "  l: % likelihood of Lava if none of previously generated adjacent tiles is Lava (integer > 0)"
       putStrLn "  ll: % likelihood of Lava when at least one of the previously-generated adjacent tiles is Lava (integer >= 0)"
       putStrLn "  x: size of each worm (integer > 0)"
       putStrLn "  y: % likelihood of Worm *in Desert* in each step (integer >= 0)"
       putStrLn "  --visible: start with visible map of size 40x40"
       putStrLn "With w + p + l <= 100 and w + p + ll <= 100"
       System.Exit.exitFailure



--
-- Main
--

-- | Ask if new or saved game.
-- If saved game
-- then list all .txt files in the saveDirectory,
--      proposes to the user to chose which one and read it.
-- else read command line arguments.
-- In both cases, if elements are correct,
-- initialize GUI
-- and start the game.
main :: IO ()

main
  = do filename <- askLoad
       loadedStr <- if filename /= ""
                    then readFile filename
                    else return ""
       let loadedParamsGame = expLinesToParamsGame $ parse loadedStr

       System.IO.hSetBuffering System.IO.stdin System.IO.NoBuffering  -- to avoid Enter requirement
       System.IO.hSetEcho System.IO.stdin False  -- to avoid printing of input character

       args <- System.Environment.getArgs
       let maybeParamsVisible = if Data.Maybe.isJust loadedParamsGame
                                then Just (fst $ Data.Maybe.fromJust loadedParamsGame, False)
                                else stringsToParams args
           maybeVisible = stringsToParams args
           maybeGame
             | Data.Maybe.isJust loadedParamsGame =
               Just (snd $ Data.Maybe.fromJust loadedParamsGame)
             | Data.Maybe.isJust maybeParamsVisible =
               Just $ createNewGame (fst $ Data.Maybe.fromJust maybeParamsVisible)
                                     (Data.Maybe.isJust maybeVisible
                                      && snd (Data.Maybe.fromJust maybeVisible))
             | otherwise = Nothing

       -- Set of available positions to move worms
       let freePositionsForWormBoxIO = Control.Concurrent.STM.newEmptyTMVarIO
       -- Set by the last worm updated or waiting
       let isFinishedWormsPhaseBoxIO = Control.Concurrent.STM.newEmptyTMVarIO
       -- Number of thread waiting after update
       let releaseThreadsBoxIO = Control.Concurrent.STM.newEmptyTMVarIO
       -- List of updated worms
       let updatedWormsBoxIO = Control.Concurrent.STM.newEmptyTMVarIO

       freePositionsForWormBox <- freePositionsForWormBoxIO
       isFinishedWormsPhaseBox <- isFinishedWormsPhaseBoxIO
       releaseThreadsBox <- releaseThreadsBoxIO
       updatedWormsBox <- updatedWormsBoxIO

       let params = fst $ Data.Maybe.fromJust maybeParamsVisible

           moveGame :: Game -> Movement -> IO Game

           moveGame game movement
             = if isLiving game
               then moveUpdate game movement
                               freePositionsForWormBox
                               isFinishedWormsPhaseBox
                               releaseThreadsBox
                               updatedWormsBox
               else return game

           handleEvent :: Gloss.Event -> Game -> IO Game

           handleEvent (Gloss.EventKey (Gloss.Char 'W') Gloss.Up _ _) game = moveGame game MoveUp
           handleEvent (Gloss.EventKey (Gloss.Char 'A') Gloss.Up _ _) game = moveGame game MoveDown
           handleEvent (Gloss.EventKey (Gloss.Char 'S') Gloss.Up _ _) game = moveGame game MoveLeft
           handleEvent (Gloss.EventKey (Gloss.Char 'D') Gloss.Up _ _) game = moveGame game MoveRight

           handleEvent (Gloss.EventKey (Gloss.Char 'w') Gloss.Up _ _) game = moveGame game MoveUp
           handleEvent (Gloss.EventKey (Gloss.Char 'a') Gloss.Up _ _) game = moveGame game MoveDown
           handleEvent (Gloss.EventKey (Gloss.Char 's') Gloss.Up _ _) game = moveGame game MoveLeft
           handleEvent (Gloss.EventKey (Gloss.Char 'd') Gloss.Up _ _) game = moveGame game MoveRight

           handleEvent (Gloss.EventKey (Gloss.SpecialKey Gloss.KeyUp) Gloss.Up _ _) game
             = moveGame game MoveUp
           handleEvent (Gloss.EventKey (Gloss.SpecialKey Gloss.KeyDown) Gloss.Up _ _) game
             = moveGame game MoveDown
           handleEvent (Gloss.EventKey (Gloss.SpecialKey Gloss.KeyLeft) Gloss.Up _ _) game
             = moveGame game MoveLeft
           handleEvent (Gloss.EventKey (Gloss.SpecialKey Gloss.KeyRight) Gloss.Up _ _) game
             = moveGame game MoveRight

           handleEvent (Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEnter) Gloss.Up _ _) game
             = do save params game $ saveDirectory ++ "/save.txt"
                  return game

           handleEvent (Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEsc) Gloss.Up _ _) _
             = System.Exit.exitSuccess

           handleEvent _ game = return game

       if Data.Maybe.isJust maybeParamsVisible
       then do images <- loadImages
               let startGame@(_, _, _, _, _, _, _, _, startWorms) = Data.Maybe.fromJust maybeGame

               -- Start thread for each initial worm
               mapM_ (\ worm -> Control.Concurrent.forkIO
                                $ concurrentUpdateWorm worm
                                                       freePositionsForWormBox
                                                       isFinishedWormsPhaseBox
                                                       releaseThreadsBox
                                                       updatedWormsBox)
                     startWorms

               Gloss.playIO (Gloss.InWindow "Desert" (windowWidth, windowHeight) (0, 0))
                             Gloss.white
                             10
                             startGame
                             (`drawAll` images)
                             handleEvent (\_ g -> return g)
       else helpUsage
