-- |
-- Module : Worm
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Type of a player, with its intrinsic information.

module Worm
  ( WormLifespan
  , Worm
  , Worms
  , createWorm
  , updateWorm
  , isHeadOrEnd
  , isAscending
  , isInWorm
  , isInWorms
  , wormDeserialize
  , wormEndIsVisible
  , wormEndPosition
  , wormEndToString
  , wormHeadIsVisible
  , wormHeadPosition
  , wormHeadToString
  , wormLifespan
  , wormSegmentToString
  , wormSerialize
  , wormVisiblePositions
  , wormVisibleSize
  ) where

import qualified Control.Exception.Base


import Console
import Position
import Map
import Tile



-- | Type for the lifespan of the worm
type WormLifespan = Int


-- | Type for worm:
-- (position of the head, first position visible, last position visible, position of the end,
-- direction of the movement, lifespan, ascending phase).
-- The position of the head and the position of the end may be have negative component.
type Worm = (Position, Position, Position, Position,
             Movement, WormLifespan, Bool)


-- | List of worms.
type Worms = [Worm]



-- | Create a worm.
createWorm :: Position -> Movement -> Int -> Worm

createWorm pos move size
  = Control.Exception.Base.assert (fst pos >= 0)
    Control.Exception.Base.assert (snd pos >= 0)
    Control.Exception.Base.assert (size > 0)
    (pos, pos, pos,
     iterate (`movePositionAlways` reverseMovement move) pos !! (size - 1),
     move, max 1 (size - 1), size > 1)



-- | True iff the position is the head or the end of the worm.
isHeadOrEnd :: Position -> Worm -> Bool

isHeadOrEnd pos worm = (pos == wormHeadPosition worm)
                       || (pos == wormEndPosition worm)



-- | True iff the worm is in its ascending phase.
isAscending :: Worm -> Bool

isAscending (_, _, _, _, _, _, ascending) = ascending



-- | True iff the position is a visible position of the worm.
isInWorm :: Position -> Worm -> Bool

isInWorm pos worm = pos `elem` wormVisiblePositions worm



-- | True iff the position is a visible position of the worm.
isInWorms :: Position -> Worms -> Bool

isInWorms pos = any (isInWorm pos)



-- | Move the Worm and update its size.
updateWorm :: Map -> Worms -> Worm -> Worm

updateWorm desertMap worms (headPos, firstVisiblePos, lastVisiblePos, endPos,
                            move, lifespan, True)
  = let newFirstVisiblePos = movePosition firstVisiblePos move
    in if positionToTile desertMap newFirstVisiblePos == Desert
          && not (isInWorms newFirstVisiblePos worms)
       then  -- valid move
         let newHeadPos@(newHeadX, newHeadY) = movePositionAlways headPos move
             newEndPos                       = movePositionAlways endPos  move
             newAscending = lifespan > 1 && newHeadX >= 0 && newHeadY >= 0
             newLifespan
               | newAscending = lifespan - 1
               | newHeadX >= 0 && newHeadY >= 0 = 1 + distanceLine newHeadPos newEndPos
               | otherwise = distanceLine newHeadPos newEndPos
         in Control.Exception.Base.assert (newHeadX >= -1)
            Control.Exception.Base.assert (newHeadY >= -1)
            Control.Exception.Base.assert (fst newFirstVisiblePos >= 0)
            Control.Exception.Base.assert (snd newFirstVisiblePos >= 0)
            Control.Exception.Base.assert (fst newEndPos >= -lifespan)
            Control.Exception.Base.assert (snd newEndPos >= -lifespan)
            Control.Exception.Base.assert (newLifespan >= 1)

            (newHeadPos, newFirstVisiblePos, lastVisiblePos, newEndPos,
              move, newLifespan, newAscending)

       else  -- invalid move
         updateWorm desertMap worms (headPos, firstVisiblePos, lastVisiblePos, endPos,
                                     move, 1 + distanceLine firstVisiblePos endPos, False)

{-|
Size 4 example:
x==|o   |       3 True
 x=|=o  |       2 True
  x|==o |       1 True

   |x==o|       4 False
   | x==|o      3 False
   |  x=|=o     2 False
   |   x|==o    1 False

   |    |x==o   0 False


Size 2 example:
x|o |     1 True

 |xo|     2 False
 | x|o    1 False
 |  |xo   0 False


Size 1 example:
|o|    1 False
| |o   0 False
|-}

updateWorm _ _ (headPos, firstVisiblePos, lastVisiblePos, endPos,
                move, lifespan, False)
  = let newHeadPos         = movePositionAlways headPos move
        newEndPos          = movePositionAlways endPos  move
        newLastVisiblePos  = if lastVisiblePos == endPos
                             then movePosition lastVisiblePos move
                             else lastVisiblePos
        newLifespan = lifespan - 1
    in Control.Exception.Base.assert (fst newHeadPos >= -1 - distanceLine headPos endPos)
       Control.Exception.Base.assert (snd newHeadPos >= -1 - distanceLine headPos endPos)
       Control.Exception.Base.assert (fst newLastVisiblePos >= 0)
       Control.Exception.Base.assert (snd newLastVisiblePos >= 0)
       Control.Exception.Base.assert (fst newEndPos >= -lifespan)
       Control.Exception.Base.assert (snd newEndPos >= -lifespan)
       Control.Exception.Base.assert (lifespan >= 0)

       (newHeadPos, firstVisiblePos, newLastVisiblePos, newEndPos,
        move, newLifespan, False)



-- | True iff the end of the worm is visible.
wormEndIsVisible :: Worm -> Bool

wormEndIsVisible (headPos, _, lastVisiblePos, endPos,
                  _, _, _)
  = lastVisiblePos == endPos && endPos /= headPos



-- | Position of the end of the worm.
wormEndPosition :: Worm -> Position

wormEndPosition (_, _, _, endPos,
                 _, _, _)
  = endPos



-- | String representation of the end.
wormEndToString :: String

wormEndToString = escBgYellow ++ escBrightGreen ++ 'x':escNormal



-- | True iff the head of the worm is visible.
wormHeadIsVisible :: Worm -> Bool

wormHeadIsVisible (headPos, firstVisiblePos, _, _,
                  _, _, _)
  = headPos == firstVisiblePos



-- | Position of the head of the worm.
wormHeadPosition :: Worm -> Position

wormHeadPosition (headPos, _, _, _,
                  _, _, _)
  = headPos



-- | String representation of the head.
wormHeadToString :: String

wormHeadToString = escBgYellow ++ escBrightGreen ++ 'o':escNormal



-- | Position of the head of the worm.
wormLifespan :: Worm -> WormLifespan

wormLifespan (_, _, _, _,
              _, lifespan, _)
  = lifespan



-- | String representation of a worm segment.
wormSegmentToString :: Movement -> String

wormSegmentToString MoveRight = escBgYellow ++ escBrightGreen ++ '=':escNormal
wormSegmentToString MoveLeft  = escBgYellow ++ escBrightGreen ++ '=':escNormal
wormSegmentToString _         = escBgYellow ++ escBrightGreen ++ 'H':escNormal



-- | Deserialize the worm, from data serialized with wormSerialize.
wormDeserialize :: Bool -> [Position] -> Worm

wormDeserialize ascending [headPos, firstVisiblePos, lastVisiblePos, endPos]
  = (headPos, firstVisiblePos, lastVisiblePos, endPos,
     recoverMove_ headPos endPos, if ascending
                                  then distanceLine lastVisiblePos endPos
                                  else 1 + distanceLine firstVisiblePos endPos,
     ascending)

wormDeserialize _ _ = error "Bad serialized data!"



-- | Serialize the worm: (ascending, the 4 characteristic positions).
wormSerialize :: Worm -> (Bool, [Position])

wormSerialize (headPos, firstVisiblePos, lastVisiblePos, endPos,
               _, lifespan, ascending)
  = (if ascending
    then Control.Exception.Base.assert (lifespan == distanceLine lastVisiblePos endPos)
    else Control.Exception.Base.assert (lifespan == 1 + distanceLine firstVisiblePos endPos))
    (ascending, [headPos, firstVisiblePos, lastVisiblePos, endPos])



-- | List of visible positions of the worm.
wormVisiblePositions :: Worm -> [Position]

wormVisiblePositions worm@(_, _, lastVisiblePos, _,
                           move, _, _)
  = loop lastVisiblePos $ wormVisibleSize worm
  where loop _ 0 = []
        loop pos size
          = pos : loop (movePosition pos move) (size - 1)



-- | Size of the visible part of the worm.
wormVisibleSize :: Worm -> Int

wormVisibleSize (_, firstVisiblePos, lastVisiblePos, _,
                   _, lifespan, _)
  =  if lifespan > 0
     then 1 + distanceLine firstVisiblePos lastVisiblePos
     else 0



--
-- Private
---

-- | Deduce the direction of the worm from its head (x0, y0) and its end (x1, y1).
recoverMove_ :: Position -> Position -> Movement

recoverMove_ (x0, y0) (x1, y1)
  | x0 == x1 && y0 <= y1 = MoveUp
  | x0 == x1 && y0 > y1 = MoveDown
  | x0 <= x1 = MoveLeft
  | otherwise = MoveRight
