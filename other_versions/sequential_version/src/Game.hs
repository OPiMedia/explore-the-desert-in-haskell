-- |
-- Module : Game
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Type and update function for the complete combination of all elements of the game.

module Game
  ( Game
  , Params
  , createNewGame
  , moveUpdate
  ) where

import qualified Data.List
import qualified Data.Set
import qualified System.Random


import Map
import Player
import Position
import Tile
import Worm



-- | All data for the game:
-- (_, _, _, maximum visible position, collected treasure positions, worm size, worm likelihood, _)
type Game = (Map, Player, Position, ViewedPositions, [Position], Int, [Bool], Worms)


-- | All initial parameters:
-- (s, m, g, t, w, p, l, ll, x, y)
type Params = (Int, Int, Int, Int, Int, Int, Int, Int, Int, Int)



-- | Return a new game with given parameters.
createNewGame :: Params -> Bool -> Game

createNewGame (s', m, g, t, w, p, l, ll, wormX, wormY) visible
  = let (seed0:seed1:_) = System.Random.randoms $ System.Random.mkStdGen g :: [Int]
        randomBools = map (< wormY)
                      (System.Random.randomRs (0, 100) $ System.Random.mkStdGen seed0)
        initialMaxVisible = if visible
                            then (40 - 1 - s', 40 - 1 - s')
                            else (0, 0)
        initialViewedPositions = Data.Set.union (Data.Set.singleton (0,0))
                                                (Data.Set.filter (\ (x, y) -> not visible
                                                                   || (x < 40) && (y < 40))
                                                 (aroundPositions (0, 0)
                                                  (if visible
                                                   then 56
                                                   else s')))
    in (randomMap seed1 (t, w, p, l, ll),
         ((0, 0), 0, m, m, s'),
         initialMaxVisible, initialViewedPositions, [],
         wormX, randomBools, [])



-- | Get the current game,
-- apply the movement to the player
-- and return the updated state of the game and True iff the state was changed.
movePlayerUpdate :: Game -> Movement -> (Game, Bool)

movePlayerUpdate game@(desertMap, player@(pos, _, _, _, _),
                       (maxVisibleX, maxVisibleY), viewedPositions, collectedTreasures,
                       wormSize, createWormBools, worms)
                 movement
  = let (updatedPos@(updatedX, updatedY), treasure, water, maxWater, radius) = move player movement
    in if updatedPos == pos  -- if impossible movement
       then (game, False)
       else let tile = positionToTile desertMap updatedPos
                isTreasure = tile == Treasure
                updatedDesertMap = if isTreasure  -- remove treasure from the map
                                   then changeTile desertMap updatedPos Desert
                                   else desertMap
                updatedCollectedTreasures = if isTreasure  -- add position to collected treasures
                                            then updatedPos:collectedTreasures
                                            else collectedTreasures
                updatedTreasure = if isTreasure  -- increment number of collected treasures
                                  then treasure + 1
                                  else treasure
                updatedWater = if tile == Water  -- fill stock of water
                               then maxWater
                               else water
                updatedViewedPositions  -- add updated viewed positions from the current updatedPos
                  = Data.Set.union viewedPositions (aroundPositions updatedPos radius)
            in ((updatedDesertMap,
                 (updatedPos, updatedTreasure, updatedWater, maxWater, radius),
                 (maximum [maxVisibleX, updatedX],  -- adapt visible zone
                  maximum [maxVisibleY, updatedY]),
                 updatedViewedPositions, updatedCollectedTreasures,
                 wormSize, createWormBools, worms),
                True)



-- | Get the current game,
-- apply the movement the player,
-- update worms, create new worms,
-- and return the updated state of the game.
moveUpdate :: Game -> Movement -> Game

moveUpdate game movement
  = let ((updatedDesertMap, updatedPlayer@(updatedPlayerPos, _, _, _, _),
          updatedMaxVisible, updatedViewedPositions, updatedCollectedTreasures,
          wormSize, createWormBools, worms),
         changed)= movePlayerUpdate game movement
    in if changed
       then let updatedWorms  -- move all worms
                  = filter ((> 0) . wormLifespan) $ map (updateWorm updatedDesertMap worms) worms

                -- Empty desert tiles in "visible" zone
                possiblePoss = Data.Set.toList
                               $ Data.Set.difference
                                 (Data.Set.difference
                                  (Data.Set.filter (\ p -> positionToTile updatedDesertMap p == Desert)
                                   (extendAllPossibleAroundPositions updatedViewedPositions wormSize))
                                  (Data.Set.singleton updatedPlayerPos))
                                 (Data.Set.unions
                                  $ map (Data.Set.fromList . wormVisiblePositions) updatedWorms)

                -- Positions where create new worms
                (newWormPositions, _) = unzip $ filter snd $ zip possiblePoss createWormBools
                updatedCreateWormBools = drop (Data.List.length possiblePoss) createWormBools

                -- Create new worms
                newWorms
                  = map (\ p -> createWorm p (betterMove_ p updatedPlayerPos) wormSize)
                        newWormPositions

            in newWorms `seq`
               (updatedDesertMap, updatedPlayer,
                updatedMaxVisible,
                updatedViewedPositions, updatedCollectedTreasures,
                wormSize, updatedCreateWormBools, updatedWorms ++ newWorms)
       else game


--
-- Private
---

-- | The "better" movement for a worm to go from (x0, y0) to (x1, y1).
betterMove_ :: Position -> Position -> Movement

betterMove_ (x0, y0) (x1, y1)
  | abs (x0 - x1) <= abs (y0 - y1) = if y0 > y1
                                     then MoveUp
                                     else MoveDown
  | x0 > x1 = MoveLeft
  | otherwise = MoveRight
