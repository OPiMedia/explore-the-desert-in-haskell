-- |
-- Module : Save
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Functions to save current state of the game.

module Save where

import qualified Control.Exception.Base
import qualified Data.List
import qualified Data.Set


import Game
import Position
import Worm



-- | Path of the directory to save (and load) game.
saveDirectory :: String

saveDirectory = "save"



-- | Return the string surrounding by brackets: [ str ]
bracketsToString :: String -> String

bracketsToString str = "[ " ++ str ++ " ]"


-- | The string representation of the current game state.
gameToString :: Params -> Game -> String

gameToString (s, m, g, t, w, p, l, ll, x, y)
             (_, (pos, _, water, maxWater, radius),
              (_, _), viewedPositions, collectedTreasures,
              wormSize, _, worms)
  = Control.Exception.Base.assert (radius == s)
    Control.Exception.Base.assert (maxWater == m)
    Control.Exception.Base.assert (wormSize == x)
    Data.List.intercalate "\n"
    $ [ "position " ++ parenthesisToString (positionToString pos)
      , "supply " ++ naturalToString water
      , "s " ++ naturalToString s
      , "m " ++ naturalToString m
      , "g " ++ naturalToString g
      , "t " ++ naturalToString t
      , "w " ++ naturalToString w
      , "p " ++ naturalToString p
      , "l " ++ naturalToString l
      , "ll " ++ naturalToString ll
      , "x " ++ naturalToString x
      , "y " ++ naturalToString y
      ]
      ++ map (("revealed " ++) . parenthesisToString . positionToString)
             (Data.List.sort $ Data.Set.toList viewedPositions)
      ++ map (("collected " ++) . parenthesisToString . positionToString)
             (Data.List.sort collectedTreasures)
      ++ map wormToString (Data.List.sort $ filter isAscending worms)
      ++ map wormToString (Data.List.sort $ filter (not . isAscending) worms)



-- | The string representation of the natural: ( n )
naturalToString :: Int -> String

naturalToString = parenthesisToString . show



-- | Return the string surrounding by parenthesis: ( str )
parenthesisToString :: String -> String

parenthesisToString str = "( " ++ str ++ " )"



-- | The string representation of the position: [ x , y ]
positionToString :: Position -> String

positionToString (x, y) = bracketsToString $ show x ++ " , " ++ show y



-- | The string representation of the positions: pos1, pos2...
positionsToString :: [Position] -> String

positionsToString positions = Data.List.intercalate " , " $ map positionToString positions



-- | The string representation of the worm.
-- See Worm.wormSerialize.
wormToString :: Worm -> String

wormToString worm@(_, _, _, _, _, _, ascending)
  = let prefix = if ascending
                 then "emerging "
                 else "disappearing "
    in prefix ++ parenthesisToString (positionsToString $ snd $ wormSerialize worm)



-- | Save the current game state in the file.
save :: Params -> Game -> FilePath -> IO ()

save params game filename = writeFile filename $ gameToString params game
