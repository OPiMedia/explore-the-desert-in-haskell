PROGS = desert

MODULES = $(wildcard *.hs) $(wildcard Libs/*.hs)

AUTOMATIC = automatic.txt

#                  s m  g   t w p l  ll x y
AUTOMATIC_PARAMS = 3 20 666 3 7 3 30 20 4 5


.SUFFIXES:

###########
# Options #
###########

HASKELL      = ghc
HASKELLFLAGS = -Wall


HP2PS      = hp2ps
HP2PSFLAGS = -c -e11.7in -s


HADDOCK      = haddock
HADDOCKFLAGS = --html --hyperlinked-source -o ../doc/html -U \
	-t "Explore the Desert (Part 2): Functional Programming project"


HLINT      = hlint
HLINTFLAGS = -r


CAT   = cat
MKDIR = mkdir -p
MV    = mv -f
RM    = rm -f
RMDIR = rmdir --ignore-fail-on-non-empty
SHELL = sh



###
# #
###
.PHONY:	all buildResult docs lint ndebug prof run runAutomatic runAutomaticVisible runVisible

all:	$(PROGS)

buildResult:	../results/desert.eps

desert:	$(MODULES)
	$(HASKELL) $(HASKELLFLAGS) -o $@ Main

docs:
	$(MKDIR) ../doc/html
	$(HADDOCK) $(HADDOCKFLAGS) $(MODULES)

lint:
	-$(HLINT) $(HLINTFLAGS) $(MODULES)

ndebug:
	$(eval HASKELLFLAGS += -O2)

prof:
	$(eval HASKELLFLAGS += -O0 -DPROFILING -prof -fprof-auto -rtsopts)

run:	desert
	#        s m  g   t w p l  ll x y
	./desert 3 20 666 3 7 3 30 20 4 1

runAutomatic:	desert
	-$(CAT) $(AUTOMATIC) | ./desert $(AUTOMATIC_PARAMS)

runAutomaticVisible:	desert
	-$(CAT) $(AUTOMATIC) | ./desert $(AUTOMATIC_PARAMS) --visible

runVisible:	desert
	#        s m  g   t w p l  ll x y
	./desert 3 20 666 3 7 3 30 20 4 1 --visible


../results/desert.hp:	distclean prof desert
	-$(CAT) $(AUTOMATIC) | ./desert +RTS -h -hcdisplayInfos -i0.01 -L32 -RTS $(AUTOMATIC_PARAMS)
	$(MV) desert.hp ../results/desert.hp



#########
# Clean #
#########
.PHONY:	clean cleanDocs distclean overclean

clean:
	$(RM) *.hi *.o Libs/*.hi Libs/*.o
	$(RM) desert.aux report.html

cleanDocs:
	$(RM) ../doc/html/src/*
	$(MKDIR) ../doc/html/src
	$(RMDIR) ../doc/html/src
	$(RM) ../doc/html/*
	$(RMDIR) ../doc/html

distclean:	clean
	$(RM) $(PROGS)

overclean:	cleanDocs distclean
	$(RM) desert.ps
