{-# LANGUAGE CPP #-}
-- |
-- Module : Main
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Explore the Desert (Part 2): Functional Programming project.

module Main where

import qualified Control.Monad

import qualified Data.Char
#ifndef PROFILING
import qualified Data.List
#endif
import qualified Data.Maybe
import qualified Data.Set

import qualified System.Environment
import qualified System.Exit
import qualified System.IO


import Console
import Game
import GameParser
import Helper
import Load
import Map
import Position
import Player
import Save
import Tile
import Worm



-- | Print the map and all current information.
display :: Game -> IO ()

display (desertMap, player@(pos, _, _, _, radius),
         maxVisiblePosition, viewedPositions, _,
         _, _, worms)
  = do clearPutStrLn ""  -- to clear the last used line when visible part of map decrease
       -- clearConsole
       goHome
#ifndef PROFILING
       displayMap desertMap pos radius maxVisiblePosition viewedPositions
#endif
       displayInfos desertMap player worms
#ifndef PROFILING
       displayHelp
       clearEndLine

       displayWorms worms viewedPositions
       goPos (0, snd maxVisiblePosition + radius + 9)

       System.IO.hFlush System.IO.stdout
#endif



-- | Print keyboard commands.
displayHelp :: IO ()

displayHelp = clearPutStrLn "Q: quit   W-A: up-down   S-D: left-right   (or arrows)   ENTER: save"



-- | Print current information.
displayInfos :: Map -> Player -> Worms -> IO ()

displayInfos desertMap (pos, treasure, water, maxWater, _) worms
  = let reachableTiles = bfsReachableTiles desertMap pos
        numberWorms = length worms
    in do clearPutStr $ show (positionToTile desertMap pos) ++ ' ':show pos
          putStr $ "   Stock water " ++ show Water ++ ':'
            : (if water < 5 then escBgRed else "") ++ ' ':show water ++ " " ++ escNormal
            ++ "<= " ++ show maxWater
          putStr $ "   Collected treasure" ++ s treasure
            ++ ' ':show Treasure ++ ": " ++ show treasure
          putStrLn $ "   Number of \"accessible\" worm" ++ s numberWorms
            ++ ' ':wormSegmentToString MoveRight ++ ": " ++ show numberWorms

#ifndef PROFILING
          clearPutStr "Reachable in n steps: "
          putStrLn $ Data.List.intercalate ", " ['[' : concatMap show tiles ++ "]"
                                                | tiles <- reachableTiles]
#endif

          clearPutStr "Shortest path "
#ifndef PROFILING
          putStr ("to lava " ++ show Lava ++ ": "
                  ++ showMaybe (findIndexContains reachableTiles Lava) ++ "   ")
#endif
          putStr ("to water " ++ show Water ++ ": "
                  ++ showMaybe (findIndexContains reachableTiles Water) ++ "   ")
          putStr ("to treasure " ++ show Treasure ++ ": "
                  ++ showMaybe (findIndexContains reachableTiles Treasure) ++ "   ")
          putStrLn ("to portal " ++ show Portal ++ ": "
                    ++ showMaybe (findIndexContains reachableTiles Portal))



-- | Print the map.
displayMap :: Map -> Position -> Int -> Position -> ViewedPositions -> IO ()

displayMap desertMap pos radius (maxVisibleX, maxVisibleY) viewedPositions
  = loop $ visibleToStrings desertMap pos (maxVisibleX + radius, maxVisibleY + radius) viewedPositions
  where loop [] = return ()
        loop (string:strings) = do clearPutStrLn string
                                   loop strings



-- | Print this quotation:
-- http://www.opimedia.be/OPiCitations/?id=3025
displayQuotation :: IO ()

displayQuotation
  = do clearPutStrLn ""
       clearPutStrLn "    \"Un homme marchait dans le désert. Une voix lui dit : « Ramasse quelques cailloux, mets-les dans ta poche. Demain, tu seras à la fois content et triste. »"
       clearPutStrLn "    L'homme obéit : le lendemain, il retira de sa poche des diamants, des rubis, des émeraudes, et il fut à la fois content et triste. Content d'avoir pris une poignée de cailloux, et triste de ne pas en avoir ramassé davantage."
       clearPutStrLn "    Ainsi en va-t-il de l'instruction.\" (C. V.)"






-- | Print all worms.
displayWorms :: Worms -> ViewedPositions -> IO ()

displayWorms [] _ = return ()

displayWorms (w:ws) viewedPositions = do displayWorm w viewedPositions
                                         displayWorms ws viewedPositions



-- | Print a worm.
displayWorm :: Worm -> ViewedPositions -> IO ()

displayWorm worm@(_, _, _, _,
                  movement, _, _) viewedPositions
  = do displayWormBodySegments worm (wormSegmentToString movement) viewedPositions
       displayWormHeadIf worm viewedPositions
       displayWormEndIf worm viewedPositions



-- | Print the end of the worm if it visible.
displayWormEndIf :: Worm -> ViewedPositions -> IO ()

displayWormEndIf worm viewedPositions
  = let pos = wormEndPosition worm
    in (Control.Monad.when (wormEndIsVisible worm && Data.Set.member pos viewedPositions)
        $ do goPosMap pos
             putStr wormEndToString)



-- | Print the head of the worm if it visible.
displayWormHeadIf :: Worm -> ViewedPositions -> IO ()

displayWormHeadIf worm viewedPositions
  = let pos = wormHeadPosition worm
    in (Control.Monad.when (wormHeadIsVisible worm && Data.Set.member pos viewedPositions)
        $ do goPosMap pos
             putStr wormHeadToString)



-- | Print a segment body of the worm.
displayWormBodySegment :: Position -> String -> ViewedPositions -> IO ()

displayWormBodySegment pos str viewedPositions
  = Control.Monad.when (Data.Set.member pos viewedPositions)
    $ do goPosMap pos
         putStr str



-- | Print all visible segments body of the worm.
displayWormBodySegments :: Worm -> String -> ViewedPositions -> IO ()

displayWormBodySegments worm str viewedPositions
  = loop $ wormVisiblePositions worm
  where loop [] = return ()
        loop (pos:poss) = do Control.Monad.unless (isHeadOrEnd pos worm)
                               $ displayWormBodySegment pos str viewedPositions
                             loop poss



-- | Print msg and terminate.
end :: String -> IO ()

end msg = do clearPutStrLn msg
             return ()



-- | If the character is 'Q', then return Nothing.
-- If the character corresponds to 'A', 'W', 'S' or 'D', then return the corresponding Movement.
-- If the character corresponds to one arrow,
-- then read next characters and return the corresponding Movement.
-- Otherwise, wait and read next character.
getMovement :: Params -> Game -> IO Char -> IO (Maybe Movement)

getMovement params game firstIoC = loop firstIoC False
  where loop :: IO Char
             -> Bool  -- ^ To deal escaped keys: arrows "\27[A", "\27[B", "\27[C" and "\27[D".
             -> IO (Maybe Movement)

        loop ioC charEscMode
          = do c <- ioC
               let upperC = Data.Char.toUpper c
               case upperC of
                 '\10' -> do save params game $ saveDirectory ++ "/save.txt"
                             putStrLn $ "Current game saved in \""
                                        ++ saveDirectory ++ "/save.txt\" file."
                             loop getChar False  -- get next char in the normal mode
                 '\27' -> loop getChar True  -- get next char in the escape mode
                 '[' -> loop getChar True    -- get next char in the escape mode
                 'Q' -> return Nothing
                 _ -> if charEscMode
                      then case upperC of
                             'B' -> return $ Just MoveDown
                             'C' -> return $ Just MoveRight
                             'D' -> return $ Just MoveLeft
                             'A' -> return $ Just MoveUp
                             _ -> loop getChar False  -- get next char in the normal mode
                      else case upperC of
                             'A' -> return $ Just MoveDown
                             'D' -> return $ Just MoveRight
                             'S' -> return $ Just MoveLeft
                             'W' -> return $ Just MoveUp
                             _ -> loop getChar False  -- get next char in the normal mode



-- | Print help message and exit.
helpUsage :: IO ()

helpUsage
  = do putStrLn "Usage: desert s m g t w p l ll [--visible]"
       putStrLn ""
       putStrLn "  s: \"radius\", length of explorer sight (integer >= 0)"
       putStrLn "  m: maximum capacity for stock of water (integer > 0)"
       putStrLn "  g: initial random seed (integer)"
       putStrLn "  t: % likelihood of Treasure *in Desert* (0 < integer <= 100)"
       putStrLn "  w: % likelihood of Water (integer > 0)"
       putStrLn "  p: % likelihood of Portal (integer > 0)"
       putStrLn "  l: % likelihood of Lava if none of previously generated adjacent tiles is Lava (integer > 0)"
       putStrLn "  ll: % likelihood of Lava when at least one of the previously-generated adjacent tiles is Lava (integer >= 0)"
       putStrLn "  x: size of each worm (integer > 0)"
       putStrLn "  y: % likelihood of Worm *in Desert* in each step (integer >= 0)"
       putStrLn "  --visible: start with visible map of size 40x40"
       putStrLn "With w + p + l <= 100 and w + p + ll <= 100"
       System.Exit.exitFailure



-- | Main event loop:
-- get Movement from keyboard,
-- update the Game
-- and display it.
play :: Params -> Game -> IO ()

play params game@(desertMap, (pos, treasure, water, _, _),
           _, _, _,
           _, _, worms)
  = let tile = positionToTile desertMap pos
    in do display game
          if water == 0
            then end "\nYou are DEAD of thirst!\7"
            else if tile == Lava
            then end "\nYou DIED burned by the lava!\7"
            else if tile == Portal
            then do displayQuotation
                    end $ "\nYou WIN with " ++ show treasure ++ " treasure" ++ s treasure ++ "."
            else if isInWorms pos worms
            then end "\nYou were KILLED by a worm!\7"
            else do maybeMove <- getMovement params game getChar
                    case maybeMove of
                      Just movement -> play params (moveUpdate game movement)
                      _ -> return ()



--
-- Main
--

-- | Ask if new or saved game.
-- If saved game
-- then list all .txt files in the saveDirectory,
--      proposes to the user to chose which one and read it.
-- else read command line arguments.
-- In both cases, if elements are correct,
-- initialize console settings
-- and start the game.
main :: IO ()

main
  = do filename <- askLoad
       loadedStr <- if filename /= ""
                    then readFile filename
                    else return ""
       let loadedParamsGame = expLinesToParamsGame $ parse loadedStr

       System.IO.hSetBuffering System.IO.stdin System.IO.NoBuffering  -- to avoid Enter requirement
       System.IO.hSetEcho System.IO.stdin False  -- to avoid printing of input character

       args <- System.Environment.getArgs
       let maybeParamsVisible = if Data.Maybe.isJust loadedParamsGame
                                then Just (fst $ Data.Maybe.fromJust loadedParamsGame, False)
                                else stringsToParams args
           maybeVisible = stringsToParams args
           maybeGame
             | Data.Maybe.isJust loadedParamsGame =
               Just (snd $ Data.Maybe.fromJust loadedParamsGame)
             | Data.Maybe.isJust maybeParamsVisible =
               Just $ createNewGame (fst $ Data.Maybe.fromJust maybeParamsVisible)
                                     (Data.Maybe.isJust maybeVisible
                                      && snd (Data.Maybe.fromJust maybeVisible))
             | otherwise = Nothing
       if Data.Maybe.isJust maybeParamsVisible
       then do clearConsole
               play (fst $ Data.Maybe.fromJust maybeParamsVisible)
                    (Data.Maybe.fromJust maybeGame)
       else helpUsage
