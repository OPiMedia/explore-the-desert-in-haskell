-- |
-- Module : Load
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Functions to load saved game.

module Load where

import qualified Data.List
import qualified Data.Set
import qualified Data.Map.Strict
import qualified Data.String.Utils
import qualified System.Directory
import qualified System.Random


import Game
import GameParser
import Helper
import Map
import Save
import Tile
import Worm



-- | Print the list of saved files (named saveDirectory ++ "/save*.txt")
-- ask which of them to load and return the filename or "".
askLoad :: IO String

askLoad = do filenames <- System.Directory.listDirectory saveDirectory
             let saves = filter (\ filename -> Data.String.Utils.startswith "save" filename
                                               && Data.String.Utils.endswith ".txt" filename)
                                filenames
             if null saves
             then return ""
             else do putStrLn $ Data.List.intercalate "\n"
                              $ map (\ (n, filename) -> show n ++ ". " ++ filename)
                                    (zip [(1::Int)..] saves)
                     putStrLn "Give the number of the saved file to load it (or other for a new game)?"
                     line <- getLine
                     let n = stringToInt line - 1
                     if 0 <= n && n < length saves
                     then return (saveDirectory ++ '/' : (saves !! n))
                     else return ""



-- | True iff all parameters are valid.
checkParams :: Params -> Bool

checkParams (s', m, _, t, w, p, l, ll, x, y)
  = (s' >= 0) && (m > 0)
    && (t > 0) && (w > 0) && (p > 0) && (l > 0) && (ll >= 0)
    && (x > 0) && (y >= 0)
    && (t <= 100)
    && (w + p + l <= 100) && (w + p + ll <= 100)



-- | Set the parameters (s, m, g, t, w, p, l, ll, x, y) from a parsed file.
-- If not enough values or if some values are not valid, then Nothing.
expLinesToParamsGame :: [ExpLine] -> Maybe (Params, Game)

expLinesToParamsGame exps
  = let intMap = foldr insertIntToMap Data.Map.Strict.empty exps
        supply = Data.Map.Strict.findWithDefault (-1) "supply"  intMap
        params@(s', m, g, t, w, p, l, ll, wormX, wormY)
          = ( Data.Map.Strict.findWithDefault (-1) "s"  intMap
            , Data.Map.Strict.findWithDefault (-1) "m"  intMap
            , Data.Map.Strict.findWithDefault (-1) "g"  intMap
            , Data.Map.Strict.findWithDefault (-1) "t"  intMap
            , Data.Map.Strict.findWithDefault (-1) "w"  intMap
            , Data.Map.Strict.findWithDefault (-1) "p"  intMap
            , Data.Map.Strict.findWithDefault (-1) "l"  intMap
            , Data.Map.Strict.findWithDefault (-1) "ll" intMap
            , Data.Map.Strict.findWithDefault (-1) "x"  intMap
            , Data.Map.Strict.findWithDefault (-1) "y"  intMap)

        positionMap = foldr insertPositionToMap Data.Map.Strict.empty exps
        positionSeq = Data.Map.Strict.findWithDefault [] "position" positionMap
        position = if length positionSeq == 1
                   then head positionSeq
                   else (-1, -1)
        revealed = Data.Map.Strict.findWithDefault [] "revealed" positionMap
        collected = Data.Map.Strict.findWithDefault [] "collected" positionMap

        positionsMap = foldr insertPositionsToMap Data.Map.Strict.empty exps
        emergings = map (wormDeserialize True)
                        (Data.Map.Strict.findWithDefault [] "emerging" positionsMap)
        disappearings = map (wormDeserialize False)
                            (Data.Map.Strict.findWithDefault [] "disappearing" positionsMap)

        player = (position, length collected, supply, m, s')

        maxVisible = (maximum (fst $ unzip revealed) - s',
                      maximum (snd $ unzip revealed) - s')

        (seed0:seed1:_) = System.Random.randoms $ System.Random.mkStdGen g :: [Int]
        randomBools = map (< wormY)
                      (System.Random.randomRs (0, 100) $ System.Random.mkStdGen seed0)

        rMap = foldr (\ treasurePosition accMap -> changeTile accMap treasurePosition Desert)
                     (randomMap seed1 (t, w, p, l, ll))
                     collected

        game = (rMap,
                player,
                maxVisible, Data.Set.fromList revealed, collected,
                wormX, randomBools, emergings ++ disappearings)

    in if checkParams params
       then Just (params, game)
       else Nothing



-- | Get the 10 parameters (s, m, g, t, w, p, l, ll, x, y) from command line arguments
-- with eventually a 11th parameter "--visible".
-- If not enough parameters or if some arguments are not valid, then Nothing.
-- See helpUsage in Main to description of these parameters.
stringsToParams :: [String] -> Maybe (Params, Bool)

stringsToParams args = if (length args /= 10) && (length args /= 11)
                       then Nothing
                       else let s' = stringToInt (head args)
                                m  = stringToInt (args !! 1)
                                g  = stringToInt (args !! 2)
                                t  = stringToInt (args !! 3)
                                w  = stringToInt (args !! 4)
                                p  = stringToInt (args !! 5)
                                l  = stringToInt (args !! 6)
                                ll = stringToInt (args !! 7)
                                x  = stringToInt (args !! 8)
                                y  = stringToInt (args !! 9)
                                visible = length args >= 11
                            in if checkParams (s', m, g, t, w, p, l, ll, x, y)
                                  && (not visible || ((args !! 10) == "--visible"))
                               then Just ((s', m, g, t, w, p, l, ll, x, y), visible)
                               else Nothing
