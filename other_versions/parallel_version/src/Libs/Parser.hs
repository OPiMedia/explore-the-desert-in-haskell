-- |
-- Module : Lib.Parser
-- Copyright : adapted from Parser by Laurent Christophe
-- Generic parser pieces.

module Libs.Parser (Parser, apply, blank, fromRead, keyword, many, oneof, sat, string) where

import qualified Control.Arrow
import qualified Control.Monad



newtype Parser a = P (String -> [(a, String)])



apply :: Parser a -> String -> [(a, String)]

apply (P f) = f



fromRead :: Read a => Parser a

fromRead = P reads



instance Functor Parser where
  fmap f p = P (map (Control.Arrow.first f) . apply p)


instance Applicative Parser where
  -- Insert the value, let the input intact
  pure x = P (\ s -> [(x, s)])
  p1 <*> p2 = P (\ s -> do (f, s1) <- apply p1 s
                           (x, s2) <- apply p2 s1
                           return (f x, s2))


instance Monad Parser where
  return = pure
  -- Combine two parsers
  p >>= f = P (concatMap (\ (x, s2) -> apply (f x) s2) . apply p)



------------------------
-- Parser combinators --
------------------------

-- | Try the first parser, in case of failure use the second parser
orelse :: Parser a -> Parser a -> Parser a

orelse p1 p2 = P (\ s -> case apply p1 s
                         of [] -> apply p2 s
                            xs -> xs)



-- | Idem orelse but on a list of parser
oneof :: [Parser a] -> Parser a

oneof = foldr orelse zero



-- | Use the same parser again and again until failure
many :: Parser a -> Parser [a]

many p = orelse
           (do x <- p
               xs <- many p
               return $ x:xs)
           (return [])



--------------------
-- Simple Parsers --
--------------------

-- | The failure parser
zero :: Parser a

zero = P (const [])



-- | Eat a character
item :: Parser Char

item = P f where f [] = []
                 f (c:cs) = [(c,cs)]



-- | Eat a character only if it satisfies
sat :: (Char -> Bool) -> Parser Char

sat f = item >>= (\ c -> if f c then return c else zero)



-- | Try to eat the given character
char :: Char -> Parser ()

char c = Control.Monad.void (sat (== c))



-- | Try to eat the given string
string :: String -> Parser ()

string = foldr ((>>) . char) (return ())



-------------
-- Helpers --
-------------

-- | Eat blanks characters
blank :: Parser ()

blank = Control.Monad.void (many (sat (\ c -> c `elem` [' ', '\t', '\n'])))


-- | Eat blank then call string parser
keyword :: String -> Parser ()

keyword s = blank >> string s
