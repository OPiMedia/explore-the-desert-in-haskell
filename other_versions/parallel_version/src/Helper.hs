-- |
-- Module : Helper
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Generic helper functions.

module Helper where

import qualified Control.Exception.Base
import qualified Data.Maybe
import qualified Text.Read



-- | "Eat" a binary function and return a empty list.
binFEmpty :: a -> b -> [c]

binFEmpty _ _ = []



-- | "Eat" a binary function and return -1.
binF_1 :: a -> b -> Int

binF_1 _ _ = -1



-- | "s" if n is at least 2, "" else.
s :: Int -> String

s n = Control.Exception.Base.assert (n >= 0)
      (if n >= 2
       then "s"
       else "")



-- | Return a string with a string representation of each item of the list,
-- separated by end of line character.
showListN :: Show a => [a] -> String

showListN [] = ""
showListN (x:xs) = show x ++ '\n' : showListN xs



-- | If Nothing then return "x", else return the string representation of the value.
showMaybe :: Show a => Maybe a -> String

showMaybe Nothing = "x"
showMaybe (Just x) = show x



-- | Square of n
square :: Int -> Int

square n = n * n



-- | Convert string to a Int.
-- But if conversion failed, then return -1.
stringToInt :: String -> Int

stringToInt string = Data.Maybe.fromMaybe (-1) (Text.Read.readMaybe string)
