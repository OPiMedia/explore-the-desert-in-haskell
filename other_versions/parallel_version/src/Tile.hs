-- |
-- Module : Tile
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Type of tile.

module Tile
  ( Tile(Desert, Lava, Portal, Treasure, Water)
  , nbTypeTile
  , tileToString
  , tileToStringIf
  ) where

import Console



-- | Type of tile.
data Tile = Desert | Lava | Portal | Treasure | Water deriving (Eq, Ord)

instance Show Tile where
  show tile = prefix_ tile ++ char_ tile : escNormal



-- | Number of different types of tile.
nbTypeTile :: Int

nbTypeTile = 5



-- | String representation of the tile (or of the player if it is on this tile).
tileToString :: Tile
             -> Bool  -- ^ the player is on this Tile?
             -> String

tileToString tile False = show tile
tileToString tile True  = prefix_ tile ++ escWhite ++ 'X':escNormal



-- | String representation of the tile if it visible,
-- else representation of a hidden tile.
tileToStringIf :: Tile
               -> Bool  -- ^ the player is on this Tile?
               -> Bool  -- ^ this tile is visible?
               -> String

tileToStringIf _ _ False = " "
tileToStringIf tile isPlayer True = tileToString tile isPlayer



--
-- Private
--

-- | Prefix_ string to deal ANSI Escape mode (if enabled).
prefix_ :: Tile -> String

prefix_ Desert   = escBgYellow
prefix_ Lava     = escBgRed
prefix_ Portal   = escBgMagenta
prefix_ Treasure = escBgYellow ++ escBlack
prefix_ Water    = escBgBlue



-- | Character representing the tile (associated with a ANSI Escape).
char_ :: Tile
      -> Char

char_ Treasure = '$'
char_ _        = ' '
