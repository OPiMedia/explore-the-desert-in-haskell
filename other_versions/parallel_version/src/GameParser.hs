-- |
-- Module : GameParser
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Functions to parse saved game.

module GameParser
  (ExpLine
  , insertIntToMap
  , insertPositionToMap
  , insertPositionsToMap
  , parse)
where

import qualified Control.Exception.Base
import qualified Control.Monad
import qualified Data.Map.Strict


import qualified Libs.Parser as Parser


import Helper
import Position



-- | Type of possible data on a line.
data ExpLine = ExpSupply Int
             | ExpS Int
             | ExpM Int
             | ExpG Int
             | ExpT Int
             | ExpW Int
             | ExpP Int
             | ExpL Int
             | ExpLl Int
             | ExpX Int
             | ExpY Int
             | ExpPosition Position
             | ExpRevealed Position
             | ExpCollected Position
             | ExpEmerging [Position]
             | ExpDisappearing [Position]
             deriving Show



-- | Eat any of characters of the string.
eatChars :: String -> Parser.Parser ()
eatChars chars = Control.Monad.void (Parser.many (Parser.sat (`elem` chars)))



-- | Insert an ExpLine "corresponding" to a Int to the map.
-- If the item already exists then replace it by -1.
insertIntToMap
  :: ExpLine -> Data.Map.Strict.Map String Int -> Data.Map.Strict.Map String Int

insertIntToMap (ExpSupply n) m = Data.Map.Strict.insertWith binF_1 "supply" n m
insertIntToMap (ExpS n)  m = Data.Map.Strict.insertWith binF_1 "s"  n m
insertIntToMap (ExpM n)  m = Data.Map.Strict.insertWith binF_1 "m"  n m
insertIntToMap (ExpG n)  m = Data.Map.Strict.insertWith binF_1 "g"  n m
insertIntToMap (ExpT n)  m = Data.Map.Strict.insertWith binF_1 "t"  n m
insertIntToMap (ExpW n)  m = Data.Map.Strict.insertWith binF_1 "w"  n m
insertIntToMap (ExpP n)  m = Data.Map.Strict.insertWith binF_1 "p"  n m
insertIntToMap (ExpL n)  m = Data.Map.Strict.insertWith binF_1 "l"  n m
insertIntToMap (ExpLl n) m = Data.Map.Strict.insertWith binF_1 "ll" n m
insertIntToMap (ExpX n)  m = Data.Map.Strict.insertWith binF_1 "x"  n m
insertIntToMap (ExpY n)  m = Data.Map.Strict.insertWith binF_1 "y"  n m
insertIntToMap _  m = Data.Map.Strict.insertWith binF_1 "_" (-1)  m



-- | "Collect" ExpLine "corresponding" to a Position to the map.
insertPositionToMap
  :: ExpLine -> Data.Map.Strict.Map String [Position] -> Data.Map.Strict.Map String [Position]

insertPositionToMap (ExpPosition  p) m = Data.Map.Strict.insertWith binFEmpty "position"  [p] m
insertPositionToMap (ExpRevealed  p) m = Data.Map.Strict.insertWith (++)      "revealed"  [p] m
insertPositionToMap (ExpCollected p) m = Data.Map.Strict.insertWith (++)      "collected" [p] m
insertPositionToMap _ m = Data.Map.Strict.insertWith binFEmpty "_" [] m



-- | "Collect" ExpLine "corresponding" to a sequence of Position to the map.
-- For item that must be unique, if the item already exists then replace it by a empty list.
insertPositionsToMap
  :: ExpLine -> Data.Map.Strict.Map String [[Position]] -> Data.Map.Strict.Map String [[Position]]

insertPositionsToMap (ExpEmerging     ps) m = Data.Map.Strict.insertWith (++) "emerging"     [ps] m
insertPositionsToMap (ExpDisappearing ps) m = Data.Map.Strict.insertWith (++) "disappearing" [ps] m
insertPositionsToMap _ m = Data.Map.Strict.insertWith binFEmpty "_" [] m



-- | Parse a string as the content of complete saved file.
parse :: String -> [ExpLine]

parse str = let r = Parser.apply parseGAME str
            in if not $ null r
               then Control.Exception.Base.assert (length r == 1)
                    Control.Exception.Base.assert (snd (head r) == "")
                    fst $ head r
               else []



-- | Parse brackets: [ parser ]
parseBracket :: Parser.Parser a -> Parser.Parser a

parseBracket parser = do Parser.keyword "["
                         inner <- parser
                         Parser.keyword "]"
                         return inner



-- | Parse a GAME.
parseGAME :: Parser.Parser [ExpLine]

parseGAME = do l <- parseLINE
               ls <- Parser.many (eatChars " \t" >> Parser.string "\n" >> parseLINE)
               return (l:ls)



-- | Parse a LINE.
parseLINE :: Parser.Parser ExpLine

parseLINE = Parser.oneof [parseLINEsupply
                         , parseLINEs, parseLINEm, parseLINEg, parseLINEt, parseLINEw
                         , parseLINEp, parseLINEl, parseLINEll, parseLINEx, parseLINEy
                         , parseLINEposition, parseLINErevealed, parseLINEcollected
                         , parseLINEemerging, parseLINEdisappearing
                         ]



-- | Parse a LINE collected.
parseLINEcollected :: Parser.Parser ExpLine

parseLINEcollected = Parser.keyword "collected"
  >> parseParenthesis (fmap ExpCollected parsePosition)



-- | Parse a LINE disappearing.
parseLINEdisappearing :: Parser.Parser ExpLine

parseLINEdisappearing = do Parser.keyword "disappearing"
                           ps <- parseParenthesis parsePositions
                           return (ExpDisappearing ps)



-- | Parse a LINE emerging.
parseLINEemerging :: Parser.Parser ExpLine

parseLINEemerging = do Parser.keyword "emerging"
                       ps <- parseParenthesis parsePositions
                       return (ExpEmerging ps)



-- | Parse a LINE g.
parseLINEg :: Parser.Parser ExpLine

parseLINEg = Parser.keyword "g" >> parseParenthesis (fmap ExpG parseNatural)



-- | Parse a LINE l.
parseLINEl :: Parser.Parser ExpLine

parseLINEl = Parser.keyword "l" >> parseParenthesis (fmap ExpL parseNatural)



-- | Parse a LINE ll.
parseLINEll :: Parser.Parser ExpLine

parseLINEll = Parser.keyword "ll" >> parseParenthesis (fmap ExpLl parseNatural)



-- | Parse a LINE m.
parseLINEm :: Parser.Parser ExpLine

parseLINEm = Parser.keyword "m" >> parseParenthesis (fmap ExpM parseNatural)



-- | Parse a LINE p.
parseLINEp :: Parser.Parser ExpLine

parseLINEp = Parser.keyword "p" >> parseParenthesis (fmap ExpP parseNatural)



-- | Parse a LINE position.
parseLINEposition :: Parser.Parser ExpLine

parseLINEposition = Parser.keyword "position"
  >> parseParenthesis (fmap ExpPosition parsePosition)



-- | Parse a LINE revealed.
parseLINErevealed :: Parser.Parser ExpLine

parseLINErevealed = Parser.keyword "revealed"
  >> parseParenthesis (fmap ExpRevealed parsePosition)



-- | Parse a LINE s.
parseLINEs :: Parser.Parser ExpLine

parseLINEs = Parser.keyword "s" >> parseParenthesis (fmap ExpS parseNatural)



-- | Parse a LINE supply.
parseLINEsupply :: Parser.Parser ExpLine

parseLINEsupply = Parser.keyword "supply"
  >> parseParenthesis (fmap ExpSupply parseNatural)



-- | Parse a LINE t.
parseLINEt :: Parser.Parser ExpLine

parseLINEt = Parser.keyword "t" >> parseParenthesis (fmap ExpT parseNatural)



-- | Parse a LINE w.
parseLINEw :: Parser.Parser ExpLine

parseLINEw = Parser.keyword "w" >> parseParenthesis (fmap ExpW parseNatural)



-- | Parse a LINE x.
parseLINEx :: Parser.Parser ExpLine

parseLINEx = Parser.keyword "x" >> parseParenthesis (fmap ExpX parseNatural)



-- | Parse a LINE y.
parseLINEy :: Parser.Parser ExpLine

parseLINEy = Parser.keyword "y" >> parseParenthesis (fmap ExpY parseNatural)



-- | Parse a natural number: n
parseNatural :: Parser.Parser Int

parseNatural = do Parser.blank
                  Parser.fromRead



-- | Parse parenthesis: ( parser )
parseParenthesis :: Parser.Parser a -> Parser.Parser a

parseParenthesis parser = do Parser.keyword "("
                             inner <- parser
                             Parser.keyword ")"
                             return inner



-- | Parse a pair of naturals: natural, natural
parsePosition :: Parser.Parser Position

parsePosition = parseBracket (do x <- parseNatural
                                 Parser.keyword ","
                                 y <- parseNatural
                                 return (x, y))



-- | Parse a sequence of pair of naturals: (natural, natural) [, (natural, natural)]*
parsePositions :: Parser.Parser [Position]

parsePositions = do p <- parsePosition
                    ps <- Parser.many (Parser.keyword "," >> parsePosition)
                    return (p:ps)
