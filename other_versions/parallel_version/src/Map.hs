-- |
-- Module : Map
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Infinite map [0..]x[0..] represented by a simple infinite list [0..] of Tile,
-- with function to generate random map, search shortest path...

module Map
  ( Map
  , bfsReachableTiles
  , changeTile
  , findIndexContains
  , positionToTile
  , randomMap
  , visibleToStrings
  ) where

import qualified Control.Exception.Base
import qualified Data.List
import qualified Data.Set
import qualified System.Random


import Position
import Tile



-- | Infinite list [0..] to represent the Infinite map [0..]x[0..].
type Map = [Tile]



-- | Explore the Map
-- and return a list of list of Tile reachable, step by step and without repetition:
-- [[tile of the current Position],
--  [tile(s) reachable with one movement from the current Position but not in previous list],
--  [tile(s) reachable with two movements from the current Position but not in previous lists],
--  ..]
bfsReachableTiles :: Map -> Position -> [[Tile]]

bfsReachableTiles tiles pos = loop (Data.Set.singleton pos) Data.Set.empty Data.Set.empty
  where loop :: Data.Set.Set Position  -- current positions
             -> Data.Set.Set Position  -- positions already visited
             -> Data.Set.Set Tile      -- all tiles already reached
             -> [[Tile]]               -- list of list of Tile reachable, by length of path

        loop positions visited reached
          = if Data.Set.null positions || Data.Set.size reached >= nbTypeTile
            then []  -- no position never visited or already all possible tiles
            else Data.Set.toList differentReachableTiles
                 : loop newPositions newVisited newReached
                 where differentReachableTiles
                         = Data.Set.difference (Data.Set.map (positionToTile tiles) positions)
                                               reached

                       newReached = Data.Set.union reached differentReachableTiles

                       newVisited = Data.Set.union visited positions

                       crossableReachablePositions
                         = Data.Set.filter (isCrossablePosition_ tiles) positions

                       newReachablePositions
                         = Data.Set.foldr
                           (Data.Set.union . Data.Set.fromList . possiblePositions)
                           Data.Set.empty
                           crossableReachablePositions

                       newPositions = Data.Set.difference newReachablePositions newVisited



-- | Return new Map with new Tile at the given Position.
changeTile :: Map -> Position -> Tile -> Map

changeTile tiles pos tile
  = let i = positionToIndex_ pos
    in take i tiles ++ tile:drop (i + 1) tiles



-- | Return the index of the first list that it contains the Tile.
-- If the list is finite and do not contains the Tile, then return Nothing.
-- With a list given by bfsReachableTiles,
-- this function give the length of the shortest path to reach the given Tile.
findIndexContains :: [[Tile]] -> Tile -> Maybe Int

findIndexContains tiless tile
  = Data.List.findIndex (elem tile) tiless



-- | Return the Tile of the given Position.
positionToTile :: Map -> Position -> Tile

positionToTile tiles (x, y) = tiles !! positionToIndex_ (x, y)



-- | Generate the infinite map randomly from the integer seed
-- and with respect to the probability parameters (in %).
randomMap :: Int -> Probas_ -> Map

randomMap seed (treasureIf, water, portal, lavaIsolated, lavaAdjacent)
  = let indices = [0..] :: [Int]
        randomPercents
          = System.Random.randomRs (0, 100) $ System.Random.mkStdGen seed :: [Int]
    in loop (zip indices randomPercents) []

       where loop :: [(Int, Int)] -> Map -> Map

             loop ((i, x):ixs) reversedTiles
               = let tile = chooseTile_
                            (treasureIf, water,
                             if i > 0 then portal else 0,        -- avoid Portal and Lava
                             if i > 0 then lavaIsolated else 0,  --   on start position
                             lavaAdjacent)
                            x [reverse reversedTiles !! prevI
                              | prevI <- indexToPreviousAdjacentIndices_ i]
                 in tile : loop ixs (tile:reversedTiles)

             loop _ _ = error "No more (index, int)!"  -- never happens, only to suppress warning



-- | Return a list of strings to represent the map.
-- (_, position of the player, maximum position to represent, _)
visibleToStrings :: Map -> Position -> Position -> ViewedPositions -> [String]

visibleToStrings tiles (x, y) (x1, y1) viewedPositions
  = let is = [0..x1]
        hrule = " +" ++ replicate (x1 + 1) '-' ++ "+"
    in (("  " ++ concat [show (i `mod` 10) | i <- is])
        : hrule
        : [concat $ show (j `mod` 10):"|":[tileToStringIf (positionToTile tiles (i, j)) ((x == i) && (y == j)) (Data.Set.member (i, j) viewedPositions)
                                          | i <- is] ++ ["|"]
          | j <- [0..y1]]
        ++ [hrule])



--
-- Private
---

-- | Probability parameters (in %) used to generated random map:
-- t: % likelihood of Treasure *in Desert* (0 < integer <= 100)
-- w: % likelihood of Water (integer > 0)
-- p: % likelihood of Portal (integer > 0)
-- l: % likelihood of Lava if none of previously generated adjacent tiles is Lava (integer > 0)
-- ll: % likelihood of Lava when at least one of the previously-generated adjacent tiles is Lava (integer > 0)
-- With w + p + l <= 100 and w + p + ll <= 100.
type Probas_ = (Int, Int, Int, Int, Int)



-- | Return a Tile corresponding to the probability parameters.
-- (_, random number in %, _)
chooseTile_ :: Probas_ -> Int -> [Tile] -> Tile

chooseTile_ (treasureIf, water, portal, lavaIsolated, lavaAdjacent) value prevTiles
  | value < water = Water
  | value - water < portal = Portal
  | otherwise = let hasAdjacentLava = elem Lava prevTiles
                    lava = if hasAdjacentLava then lavaAdjacent else lavaIsolated
                in if value - water - portal < lava
                   then Lava
                   else if (value - water - portal - lava) * (100 - water - portal - lava)
                           < treasureIf*100
                        then Treasure
                        else Desert



-- | (x, y) position in the map from the index in the list.
indexToPosition_ :: Int -> Position

indexToPosition_ i
  = let diag = (floor $ ((sqrt (fromIntegral (i*8 + 1))::Double) - 1)/2) :: Int
        y = i - positionToIndex_ (diag, 0)
    in Control.Exception.Base.assert (i >= 0)
       (diag - y, y)



-- | List of indices strictly less than the given index
-- and such that they correspond to adjacent position on the map.
indexToPreviousAdjacentIndices_ :: Int -> [Int]

indexToPreviousAdjacentIndices_ i = [ positionToIndex_ pos
                                    | pos <- previousAdjacentPositions_ (indexToPosition_ i)]



-- | True iff the tile at the position is crossable, i.e. it is not Lava or Portal.
isCrossablePosition_ :: Map -> Position -> Bool

isCrossablePosition_ tiles pos = let tile = positionToTile tiles pos
                                 in (tile /= Lava) && (tile /= Portal)



-- | Index in the list from the (x, y) position in the map.
positionToIndex_ :: Position -> Int

positionToIndex_ (x, y)
  = Control.Exception.Base.assert (x >= 0)
    Control.Exception.Base.assert (y >= 0)
    ((x + y)*(x + y + 1) `div` 2 + y)



-- | List of "previous" adjacent Position to the given Position.
previousAdjacentPositions_ :: Position -> [Position]

previousAdjacentPositions_ (x, y)
  = [(x', y') | (x', y') <- [(x - 1, y - 1), (x, y - 1), (x + 1, y - 1), (x - 1, y)],
                x' >= 0, y' >= 0]
