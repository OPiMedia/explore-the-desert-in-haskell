-- |
-- Module : Console
-- Copyright : (c) Olivier Pirson --- http://www.opimedia.be/
-- License : GPLv3
-- Strings to make some cool stuffs with the console (colors, clear, etc.)
-- http://ascii-table.com/ansi-escape-sequences.php
-- https://en.wikipedia.org/wiki/ANSI_escape_code

module Console where

import Position



-- | Background black.
escBgBlack :: String
escBgBlack = "\27[40m"

-- | Background blue.
escBgBlue :: String
escBgBlue = "\27[44m"

-- | Background magenta.
escBgMagenta :: String
escBgMagenta = "\27[45m"

-- | Background red.
escBgRed :: String
escBgRed = "\27[41m"

-- | Background Yellow.
escBgYellow :: String
escBgYellow = "\27[43m"



-- | Foreground black.
escBlack :: String
escBlack = "\27[30m"

-- | Foreground bright green.
escBrightGreen :: String
escBrightGreen = "\27[92m"

-- | Foreground white.
escWhite :: String
escWhite = "\27[37m"



-- | Erase the remain line.
escEndLine :: String
escEndLine = "\27[K"



-- | Set all attributes off.
escNormal :: String
escNormal = "\27[0m"



-- | Erase all the display.
clearConsole :: IO ()

clearConsole = putStr "\27[2J"



-- | Erase the remain line.
clearEndLine :: IO ()

clearEndLine = putStr escEndLine



-- | Erase the remain line and put the string with an end of line.
clearPutStr :: String -> IO ()

clearPutStr s = do clearEndLine
                   putStr s



-- | Erase the remain line and put the string with an end of line.
clearPutStrLn :: String -> IO ()

clearPutStrLn s = do clearEndLine
                     putStrLn s



-- | Move cursor to position (0, 0).
goHome :: IO ()

goHome = putStr "\27[H"



-- | Move cursor to position (x, y).
goPos :: Position -> IO ()

goPos (x, y) = putStr $ "\27[" ++ show y ++ ';' : show x ++ "H"



-- | Move cursor to corresponding map position.
goPosMap :: Position -> IO ()

goPosMap (x, y) = putStr $ "\27[" ++ show (y + 3) ++ ';' : show (x + 3) ++ "H"
